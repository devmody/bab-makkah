import React from "react";
import { Radio, Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, NavItem, Navbar, Carousel, Modal, FormGroup, FormControl, ControlLabel } from 'react-bootstrap'
import Select from 'react-select';
import { findIndex } from 'lodash';

class CustomModal extends React.Component {

    constructor() {
        super();
        this.determineValue = this.determineValue.bind(this)
    }
    determineType(field) {
        if (field === 'email')
            return "email"

        return "text"
    }
    determineValue(field) {
        let shippingOrBilling = this.props.shippingOrBilling

        if (shippingOrBilling) {
            if (!this.props.user[shippingOrBilling]) {
                return ''
            }
            return this.props.user[shippingOrBilling][field]
        }
        return this.props.user[field]
    }
    render() {
        let nationalities = [
            "Afghan",
            "Albanian",
            "Algerian",
            "American",
            "Andorran",
            "Angolan",
            "Antiguans",
            "Argentinean",
            "Armenian",
            "Australian",
            "Austrian",
            "Azerbaijani",
            "Bahamian",
            "Bahraini",
            "Bangladeshi",
            "Barbadian",
            "Barbudans",
            "Batswana",
            "Belarusian",
            "Belgian",
            "Belizean",
            "Beninese",
            "Bhutanese",
            "Bolivian",
            "Bosnian",
            "Brazilian",
            "British",
            "Bruneian",
            "Bulgarian",
            "Burkinabe",
            "Burmese",
            "Burundian",
            "Cambodian",
            "Cameroonian",
            "Canadian",
            "Cape Verdean",
            "Central African",
            "Chadian",
            "Chilean",
            "Chinese",
            "Colombian",
            "Comoran",
            "Congolese",
            "Costa Rican",
            "Croatian",
            "Cuban",
            "Cypriot",
            "Czech",
            "Danish",
            "Djibouti",
            "Dominican",
            "Dutch",
            "East Timorese",
            "Ecuadorean",
            "Egyptian",
            "Emirian",
            "Equatorial Guinean",
            "Eritrean",
            "Estonian",
            "Ethiopian",
            "Fijian",
            "Filipino",
            "Finnish",
            "French",
            "Gabonese",
            "Gambian",
            "Georgian",
            "German",
            "Ghanaian",
            "Greek",
            "Grenadian",
            "Guatemalan",
            "Guinea-Bissauan",
            "Guinean",
            "Guyanese",
            "Haitian",
            "Herzegovinian",
            "Honduran",
            "Hungarian",
            "I-Kiribati",
            "Icelander",
            "Indian",
            "Indonesian",
            "Iranian",
            "Iraqi",
            "Irish",
            "Israeli",
            "Italian",
            "Ivorian",
            "Jamaican",
            "Japanese",
            "Jordanian",
            "Kazakhstani",
            "Kenyan",
            "Kittian and Nevisian",
            "Kuwaiti",
            "Kyrgyz",
            "Laotian",
            "Latvian",
            "Lebanese",
            "Liberian",
            "Libyan",
            "Liechtensteiner",
            "Lithuanian",
            "Luxembourger",
            "Macedonian",
            "Malagasy",
            "Malawian",
            "Malaysian",
            "Maldivan",
            "Malian",
            "Maltese",
            "Marshallese",
            "Mauritanian",
            "Mauritian",
            "Mexican",
            "Micronesian",
            "Moldovan",
            "Monacan",
            "Mongolian",
            "Moroccan",
            "Mosotho",
            "Motswana",
            "Mozambican",
            "Namibian",
            "Nauruan",
            "Nepalese",
            "New Zealander",
            "Nicaraguan",
            "Nigerian",
            "Nigerien",
            "North Korean",
            "Northern Irish",
            "Norwegian",
            "Omani",
            "Pakistani",
            "Palauan",
            "Panamanian",
            "Papua New Guinean",
            "Paraguayan",
            "Peruvian",
            "Polish",
            "Portuguese",
            "Qatari",
            "Romanian",
            "Russian",
            "Rwandan",
            "Saint Lucian",
            "Salvadoran",
            "Samoan",
            "San Marinese",
            "Sao Tomean",
            "Saudi",
            "Scottish",
            "Senegalese",
            "Serbian",
            "Seychellois",
            "Sierra Leonean",
            "Singaporean",
            "Slovakian",
            "Slovenian",
            "Solomon Islander",
            "Somali",
            "South African",
            "South Korean",
            "Spanish",
            "Sri Lankan",
            "Sudanese",
            "Surinamer",
            "Swazi",
            "Swedish",
            "Swiss",
            "Syrian",
            "Taiwanese",
            "Tajik",
            "Tanzanian",
            "Thai",
            "Togolese",
            "Tongan",
            "Trinidadian or Tobagonian",
            "Tunisian",
            "Turkish",
            "Tuvaluan",
            "Ugandan",
            "Ukrainian",
            "Uruguayan",
            "Uzbekistani",
            "Venezuelan",
            "Vietnamese",
            "Welsh",
            "Yemenite",
            "Zambian",
            "Zimbabwean"
        ]
        nationalities = nationalities.map((nat) => ({ value: nat, label: nat }))
        if (Object.keys(this.props.user).length === 0)
            return (<div />)
        return (
            <Modal className="edit-account"
                show={this.props.show}
                onHide={this.props.handleHide}
                aria-labelledby="contained-modal-title"
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title">
                        {this.props.strings.edit}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Row>
                        <form onSubmit={this.props.handleSubmit}>
                            {this.props.fields.map((field, index) => (
                                field === 'gender' ?
                                    <Col md={12}>
                                        <FormGroup controlId="formValidationNull">
                                            <ControlLabel>{this.props.strings.gender}</ControlLabel>
                                            <Radio checked={this.props.user.gender + "" === "1"} value={1} onChange={this.props.handleChange} name="gender" inline>
                                                {this.props.strings.male}
                                            </Radio>
                                            <Radio checked={this.props.user.gender + "" === "0"} value={0} onChange={this.props.handleChange} name="gender" inline>
                                                {this.props.strings.female}
                                            </Radio>
                                        </FormGroup>
                                    </Col>
                                    :
                                    field === 'country' ?
                                        (
                                            <React.Fragment>
                                                <Col md={12}>
                                                    <FormGroup>
                                                        <ControlLabel>{this.props.strings.country}</ControlLabel>
                                                        <Select
                                                            clearable={false}
                                                            placeholder="Select your country"
                                                            value={this.props.countries[findIndex(this.props.countries, { label: this.props.user[this.props.shippingOrBilling].country })]}
                                                            isLoading={this.props.user[this.props.shippingOrBilling].country && this.props.countries.length === 0}
                                                            onChange={this.props.handleCountryChange}
                                                            options={this.props.countries}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                                <Col md={12}>
                                                    <FormGroup>
                                                        <ControlLabel>{this.props.strings.city}</ControlLabel>
                                                        
                                                        <Select
                                                            clearable={false}
                                                            placeholder="Select your city"
                                                            value={this.props.cities[findIndex(this.props.cities, { label: this.props.user[this.props.shippingOrBilling].city })]}
                                                            isLoading={this.props.user[this.props.shippingOrBilling].country && this.props.cities.length === 0}
                                                            onChange={this.props.handleCityChange}
                                                            options={this.props.cities}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                            </React.Fragment>

                                        )
                                        :
                                        field === 'nationality' ?
                                            <Col md={12}>
                                                <FormGroup controlId="formValidationNull">
                                                    <ControlLabel>{this.props.strings.nationality}</ControlLabel>
                                                    <Select
                                                        className="basic-single"
                                                        classNamePrefix="select"
                                                        onChange={this.props.handleChangeNationality}
                                                        value={this.props.user.nationality ? ({ label: this.props.user.nationality, value: this.props.user.nationality }) : ('')}
                                                        name="nationality"
                                                        options={nationalities}
                                                    />
                                                </FormGroup>
                                            </Col>
                                            :
                                            <Col md={12}>
                                                <FormGroup controlId="formValidationNull">
                                                    <ControlLabel>{this.props.strings[field]}</ControlLabel>
                                                    <FormControl onChange={this.props.handleChange} name={field} value={this.determineValue(field)} type={this.determineType(field)} placeholder={this.props.strings[field]} />
                                                </FormGroup>
                                            </Col>
                            ))}
                            <Col md={6}>
                                <Button onClick={this.props.handleHide} className="cancel">{this.props.strings.cancel}</Button>
                            </Col>
                            <Col md={6}>
                                <Button type="submit" className="save main-btn">{this.props.strings.save}</Button>
                            </Col>
                        </form>
                    </Row>
                </Modal.Body>
            </Modal>
        )
    }

}
export default CustomModal;

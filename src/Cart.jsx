import { Container } from 'unstated';
import { getObjectWithToken } from './services/CommonServices';

class CartContainer extends Container {

    constructor() {
        super();
        this.state = {
            count: 0,
            total: 0,
            name: '',
            loggedIn: false
        }
        this.refreshCart()
    }
    refreshCart = () => {
        getObjectWithToken('users/profile')
            .then((response) => {
                this.setState({
                    count: response.data.cart.products.reduce((accumulator, cartObject) => { return accumulator + cartObject.quantity }, 0),
                    total: response.data.cart.products.reduce((accumulator, cartObject) => { return accumulator + cartObject.product.sizes[cartObject.size].price }, 0),
                    name: response.data.name,
                    loggedIn: true
                })
            })
            .catch((err) => {
                localStorage.removeItem("token")
                this.setState({
                    count: 0,
                    total: 0,
                    name: '',
                    loggedIn: false
                }, () => {
                    let cart = localStorage.getItem("cart")
                    if (cart) {
                        cart = JSON.parse(cart)
                        this.setState({
                            count: cart.reduce((accumulator, cartObject) => { return accumulator + cartObject.quantity }, 0),
                        })
                    }
                })
            })
    }
}

export default CartContainer
let config = {}
if (process.env.NODE_ENV === "development") {
    config = {
        API_URL: 'http://localhost:3350/',
        SHARING_URL: 'http://localhost:3350/',
        CLDNRY_ID: "qpixio",
        MC_URL: 'https://qpix.us12.list-manage.com/subscribe/post-json?u=01ccf6f4a23736fe89dbb8955&id=441ebbb90a',
    }
} else {
    config = { //=== "production"
        API_URL: 'https://api.bab-makkah.qpix.io/',
        SHARING_URL: 'https://api.bab-makkah.qpix.io/',
        CLDNRY_ID: "qpixio",
        MC_URL: 'https://qpix.us12.list-manage.com/subscribe/post-json?u=01ccf6f4a23736fe89dbb8955&id=441ebbb90a',
    }
}

config.reasons= {
    1: 'Redemption',
    2: 'Offline Redemption'
}

module.exports = config;
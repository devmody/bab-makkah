import axios from 'axios';
import config from '../config';
var qs = require('qs');

// function checkToken(err){
//     if(localStorage.getItem("token")){
//         localStorage.removeItem("token")
//         window.location.href="/register"
//     }
//     throw "Error"    
// }

export function getAllObjects(objectName, query) {
    return axios.get(config.API_URL + objectName, {
        params: query
    })
}

export function getWithQuery(objectName, url, query) {
    const queryString = qs.stringify(query)
    return axios({
        method: 'GET',
        baseURL: config.API_URL + objectName + '/',
        url: url + "?" + queryString,
    })
}
export function getSingleObject(objectName, object_id) {
    return axios({
        method: 'GET',
        baseURL: config.API_URL + objectName + '/' + object_id
    })
}

export function getObjectWithToken(objectName) {
    var options = {
        headers: {
            'Content-Type': 'application/json',
            'x-access-token': localStorage.getItem('token')
        }
    }
    return axios(config.API_URL + objectName, options)
}

export function postObjectWithToken(objectName, body, progress) {
    var options = {
        headers: {
            'Content-Type': 'application/json',
            'x-access-token': localStorage.getItem('token')
        }
    }
    return axios.post(config.API_URL + objectName + '/', body, options)
}


export function postObject(objectName, body) {
    return axios.post(config.API_URL + objectName + '/', body)
}

export function deleteObject(objectName, body) {
    return axios({
        method: 'DELETE',
        url: body._id,
        baseURL: config.API_URL + objectName + '/',
        headers: {
            'Content-Type': 'application/json',
            'x-access-token': localStorage.getItem('token')
        }
    })
}

export function updateObject(objectName, query, body) {
    body["_id"] = query._id
    return axios({
        method: 'PUT',
        url: config.API_URL + objectName,
        data: body,
    })
}


export function updateObjectWithToken(objectName, body) {
    var headers = {
        'Content-Type': 'application/json',
        'x-access-token': localStorage.getItem('token')
    }
    return axios({
        method: 'PUT',
        url: config.API_URL + objectName,
        data: body,
        headers
    })
}

export function closeNotification(context) {
    setTimeout(function () {
        context.setState({
            isShowing: false,
        })
    }, 3000)
}

export function handleCatch(err, context) {
    context.setState({
        message: err.message,
        isShowing: true,
        status: 'ERROR!',
        alert: 'danger'
    })
}

export function toPrice(price) {
    if (!price) return null
    return price.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
}

export function calculateRatingAverage(reviews) {
    let average = 0;
    for (let i = 0; i < reviews.length; i++) {
        average += reviews[i].rating;
    }
    if (average === 0) return 0;
    return average / reviews.length
}

export function getDesign(segmentId) {
    const designKey = segmentId ? (segmentId.split(".")[1]) : ('')
    return (config.designs[designKey] ? (config.designs[designKey]) : (''))
}
// export function toBoxPrice(product,price) {
//     let boxSize = 1
//     if(product.boxSize){
//         boxSize = product.boxSize;
//     }
//     if(boxSize === 1 ){
//         return price;
//     }
//     return price * (product.size.length/100) * (product.size.width/100) * boxSize
// }

export function toBoxPrice(product, price) {
    let boxSize = 1
    if (product.boxSizeMeter) {
        boxSize = product.boxSizeMeter;
    }
    if (boxSize === 1) {
        return price;
    }
    return price * boxSize
}

export function toCloudinary(imageUrl, height, width) {
    if (!imageUrl) return null
    let image = imageUrl.trim()
    if (!image) return null
    let transformation = ""
    if (height) {
        transformation += "h_" + height + (width ? (",") : (""))
    }
    if (width) {
        transformation += "w_" + width
    }
    return "https://res.cloudinary.com/" + config.CLDNRY_ID + "/image/fetch/" + transformation + "/" + image
}

export function addToLocalCart(cartItem, cb) {
    let cart = localStorage.getItem("cart");
    if (!cart) {
        localStorage.setItem("cart", JSON.stringify([]))
        cart = []
    } else {
        cart = JSON.parse(cart)
    }
    let found = false
    cart.forEach((item, index) => {
        if (item.product._id === cartItem.product._id) {
            cart[index].quantity += cartItem.quantity;
            found = true;
        }
    })
    if (!found) {
        cart.push(cartItem);
    }
    localStorage.setItem("cart", JSON.stringify(cart))
    if (cb) {
        cb()
    }
}

export function removeFromLocalCart(index, cb) {
    let cart = localStorage.getItem("cart");
    if (!cart) {
        return
    } else {
        cart = JSON.parse(cart)
    }
    cart.splice(index, 1);
    localStorage.setItem("cart", JSON.stringify(cart))
    if (cb) {
        cb()
    }
}

export function transferCart(userCart,cb) {
    if (!userCart || !localStorage.getItem("cart")) {
        if (cb) {
            cb()
        }
        return;
    }
    if (userCart.length === 0) {
        updateObjectWithToken('users/all-cart', {
            products: JSON.parse(localStorage.getItem("cart"))
        }).then((response) => {
            console.log(response)
        })
    }
    localStorage.removeItem("cart");
    if (cb) {
        cb()
    }
}
module.exports.tempProduct = {
    name: {
        en: 'Loading',
        ar: 'جارى التحميل'
    },
    type: {
        name: {
            en: 'Temp type',
            ar: 'نوع'
        },
        category:{
            name:{
                en:'Temp category'
            }
        }
    },
    description:{
        en:''
    },
    vendor:{
        name:{
            en:''
        }
    },
    sizesUnit:{
        en:''
    },
    sizes:[
        {name:{},price:0}
    ],
    images:[],
    reviews:[],
    colors:[]
}

module.exports.tempCategory = {
    name: {
        en: 'Loading',
        ar: 'جارى التحميل'
    }
}
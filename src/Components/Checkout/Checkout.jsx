import React from "react";
import { Row, Col, Grid, Button, Radio, ControlLabel, FormGroup, Checkbox, FormControl, Modal } from 'react-bootstrap'
import ShopCategoryInside from "../ShopCategoryInside/ShopCategoryInside";
import { updateObjectWithToken, postObjectWithToken, getObjectWithToken, getAllObjects, toPrice } from "../../services/CommonServices";
import ProductItem from "../ProductItem/ProductItem";
import { toast, Flip } from 'react-toastify';

import { get } from 'lodash'
import { Subscribe } from 'unstated';
import CartContainer from "../../Cart";
import ReactPhoneInput from 'react-phone-input-2';
import queryString from 'query-string';
import Steps, { Step } from 'rc-steps';
import 'react-select/dist/react-select.css'
import 'react-virtualized-select/styles.css'
// Then import the virtualized Select HOC
import Select from 'react-virtualized-select'
import 'rc-steps/assets/index.css';

import 'rc-steps/assets/iconfont.css';
import { findIndex } from 'lodash';
import NotificationSystem from "react-notification-system";


var countries = require("i18n-iso-countries");
countries.registerLocale(require("i18n-iso-countries/langs/en.json"));
const cashOnDelivery = 18
const toastSettings = {
	position: "top-right",
	autoClose: 5000,
	transition: Flip,
	hideProgressBar: true,
	closeOnClick: true,
	pauseOnHover: true,
	draggable: false,
	className: "sub-toast"
}
class Checkout extends React.Component {
	toastSettings = {
        position: "top-right",
        autoClose: 5000,
        transition: Flip,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        className: "sub-toast"
    }
	constructor() {
		super()
		this.onChangetext = this.onChangetext.bind(this)
		this.saveAddress = this.saveAddress.bind(this)
		this.countingCashOnDelivery = this.countingCashOnDelivery.bind(this)
		this.state = {
			user: {
				shipping: {}, billing: {},
				name: '',
				phone_number: {
					number: '',
					country_code: ''
				},
			},
			cashOnDeliveryCount: 1,
			email: '',
			accept: "off",
			countries: [],
			cities: {
				shipping: [],
				billing: []
			},
			userInfos: {
				shipping: ["address_1", "address_2", "area"],
				billing: ["address_1", "address_2", "area"],
			},
			cc_phone_number: '00966',
			cc_promocode: '',
			orders: [],
			cc_discount: 0,
			total_after_discount: 0
		}
	}

	

	 validateEmail = (email) => {
		let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	  }
	  
	saveAddress() {
		let username = this.state.cc_first_name + " " + this.state.cc_last_name
		let user = JSON.parse(JSON.stringify(this.state.user))
		user.name = username
		this.setState({
			user: user
		})

		delete user.phone_number
		updateObjectWithToken('users/profile', user)
			.then((response) => {
				toast.success(this.props.strings.saveAddressSuccess, toastSettings)
			})
			.catch((error) => {
				toast.error(this.props.strings.error, toastSettings)
			})
	}
	handleOnlinePayment = (query, shipping, billing) => {
		postObjectWithToken('order/pay-page?' + query, { shipping: shipping, billing: billing, email: this.state.email, cc_discount: this.state.cc_discount  })
			.then((response) => {
				window.location.href = response.data.payment_url
			})
			.catch((err) => {
				toast.error(err.response.data.message, toastSettings)
			})
	}
	handleOfflinePayment = (query, shipping, billing) => {
		postObjectWithToken('order?' + query, { shipping: shipping, billing: billing, email: this.state.email, cc_discount: this.state.cc_discount })
			.then((response) => {
				toast.success("Order complete", toastSettings)
				this.props.history.push({
					pathname: '/account-orders'
				})
			})
			.catch((err) => {
				toast.error(err.response.data.message, toastSettings)
			})
	}
	refreshShipping = () => {
		this.setState({ shippingTotal: null, shippingError: this.props.strings.calculating }, () => {
			getObjectWithToken('order/calculate-shipping?destinationCity=' + this.state.user.shipping.city + '&destinationCountry=' + this.state.user.shipping.country_code + '&zip=' + this.state.user.shipping.zip + (this.state.method ? ('&method=' + this.state.method) : ('')))
				.then((response) => {
					this.setState({ shippingTotal: response.data.total, shippingError: null })
					//console.log('shippingTotalll', response.data.total, this.state.shippingTotal)
				})
				.catch((err) => {
					console.log(err.response)
					this.setState({ shippingError: this.props.strings.shippingUnav })
				})
		})
	}
	placeOrder = () => {
		if (!this.state.method) {
			toast.error(this.props.strings.selectPayment, toastSettings)
			return;
		}
		if (this.state.method !== 'online') {
			if (this.state.user.shipping.country_code !== "SA") {
				return toast.error(this.props.strings.cashSaudi, toastSettings)
			}
		}
		if (!this.state.accept || this.state.accept === 'off') {
			toast.error(this.props.strings.termsMsg, toastSettings)
			return
		}

		if (!this.state.email) {
			toast.error("Email is required", toastSettings)
			return;
		}
		else if( !(this.validateEmail(this.state.email))){
			toast.error("enter a valid email schema", toastSettings)
			return;
		}
		const requireds = ["cc_first_name", "cc_last_name", "cc_phone_number", "phone_number", "billing_address", "city", "state", "postal_code", "country", "address_shipping", "city_shipping", "state_shipping", "postal_code", "postal_code_shipping", "country_shipping"]
		const shippingOrBilling = this.state.sameAsBilling ? ("shipping") : ("billing")
		let orderBody = {
			cc_first_name: this.state.cc_first_name,
			cc_last_name: this.state.cc_last_name,
			cc_phone_number: this.state.cc_phone_number,
			phone_number: this.state.phone_number.replace(this.state.cc_phone_number.replace('00', '+'), ''),

			address_shipping: get(this.state.user.shipping, "address_1", ""),
			billing_address: get(this.state.user, [shippingOrBilling, "address_1"], ""),

			country_shipping: countries.getAlpha3Code(get(this.state.user.shipping, "country", ""), 'en'),
			country: countries.getAlpha3Code(get(this.state.user, [shippingOrBilling, "country"], ""), 'en'),

			city_shipping: get(this.state.user.shipping, "area", ""),
			city: get(this.state.user, [shippingOrBilling, "area"], ""),

			state_shipping: get(this.state.user.shipping, "city", ""),
			state: get(this.state.user, [shippingOrBilling, "city"], ""),

			postal_code_shipping: get(this.state.user.shipping, "zip", ""),
			postal_code: get(this.state.user, [shippingOrBilling, "zip"], "")
		}
		for (let i = 0; i < requireds.length; i++) {
			if (!orderBody[requireds[i]]) {
				toast.error(this.props.unenteredValues, toastSettings)
				return;
			}
		}
		let shipping = {
			address_1: this.state.user.shipping ? this.state.user.shipping.address_1 : '',
			address_2: this.state.user.shipping ? this.state.user.shipping.address_2 : '',
			area: this.state.user.shipping ? this.state.user.shipping.area : '',
			city: this.state.user.shipping ? this.state.user.shipping.city : '',
			country: this.state.user.shipping ? this.state.user.shipping.country : '',
			country_code: this.state.user.shipping ? this.state.user.shipping.country_code : '',
			phone: this.state.phone_number,
			zip: this.state.user.shipping ? this.state.user.shipping.zip : '',
		}
		let billing = {}
		if (this.state.sameAsBilling) {
			billing = JSON.parse(JSON.stringify(shipping))
		} else {
			billing = {
				address_1: this.state.user.billing ? this.state.user.billing.address_1 : '',
				address_2: this.state.user.billing ? this.state.user.billing.address_2 : '',
				area: this.state.user.billing ? this.state.user.billing.area : '',
				city: this.state.user.billing ? this.state.user.billing.city : '',
				country: this.state.user.billing ? this.state.user.billing.country : '',
				country_code: this.state.user.billing ? this.state.user.billing.country_code : '',
				phone: this.state.phone_number,
				zip: this.state.user.billing ? this.state.user.billing.zip : '',
			}
			// shipping = {
			// 	address_1: this.state.user.shipping ? this.state.user.shipping.address_1 : '',
			// 	address_2: this.state.user.shipping ? this.state.user.shipping.address_2 : '',
			// 	area: this.state.user.shipping ? this.state.user.shipping.area : '',
			// 	city: this.state.user.shipping ? this.state.user.shipping.city : '',
			// 	country: this.state.user.shipping ? this.state.user.shipping.country : '',
			// 	country_code: this.state.user.shipping ? this.state.user.shipping.country_code : '',
			// 	phone: this.state.user.phone_number,
			// 	zip: this.state.user.billing ? this.state.user.shipping.zip : '',
			// }
		}
		toast(this.props.strings.procc, toastSettings)
		let query = queryString.stringify(orderBody)
		if (this.state.method === "online") {
			this.handleOnlinePayment(query, shipping, billing)
		} else {
			this.handleOfflinePayment(query, shipping, billing)
		}

	}
	getCities = (country, objectType) => {
		const cities = Object.assign({}, this.state.cities)
		cities[objectType] = []
		this.setState({ cities: cities }, () => {
			getAllObjects('order/cities?country=' + country)
				.then((response) => {
					this.setState({ cities: Object.assign(this.state.cities, { [objectType]: response.data.map((c) => ({ value: c, label: c })) }) })
				})
				.catch((err) => {
					console.log(err)
				})
		})
	}
	onChangetext(e, userInfo, attribute) {
		let user = Object.assign({}, this.state.user)
		if (!user[userInfo])
			user[userInfo] = {}
		user[userInfo][attribute] = e.target.value
		let object = Object.assign({}, this.state[userInfo])
		object[attribute] = e.target.value
		this.setState({ [userInfo]: object, user: user })
	}
	handlePhoneChange = (phoneNumber, countryDetails) => {
		this.setState({ cc_phone_number: "00" + countryDetails.dialCode, phone_number: phoneNumber })
	}
	componentDidMount = () => {

		var outside = this
		console.log(this.props)
		if (!this.props.location.state) {
			this.props.history.push({ pathname: '/cart' })
		}

		getObjectWithToken('order/countries')
			.then((response) => {
				this.setState({ countries: response.data.map((c) => ({ value: c, label: c.Name })) }, () => {
					getObjectWithToken('users/profile')
						.then((response) => {
							console.log("order countries ", response.data)

							this.setState({ email: response.data.email, cc_phone_number: "00" + response.data.phone_number.country_code, user: response.data, cc_first_name: response.data.name.split(' ').slice(0, 1), cc_last_name: response.data.name.split(' ').slice(1), phone_number: "+" + response.data.phone_number.country_code + response.data.phone_number.number }, () => {
								this.countingCashOnDelivery()
								this.refreshShipping()
								if (response.data.shipping.country)
									this.getCities(this.state.countries.find((c) => c.value.Name === response.data.shipping.country).value.Code, 'shipping');
								if (response.data.billing.country)
									this.getCities(this.state.countries.find((c) => c.value.Name === response.data.billing.country).value.Code, 'billing')
							})
						})
						.catch(err => {
							console.log(err)
						})
				})
			})
			.catch((err) => {
				console.log(err)
			})


		getObjectWithToken('order/users')
			.then((response) => {
				this.setState({ orders: response.data })
				console.log("this is order", this.state.orders)
			})

			.catch((err) => {
				console.log(err)
			})



	}
	countingCashOnDelivery = () => {
		let count = 1;
		let history=[]

		
	
			for (let i = 0; i < this.state.user.cart.products.length; i++) {
				let flag = true;
				for (let j = 0 ; j < history.length ; j++){
					console.log("history of j : ",history[j]);
					console.log("vendor : ",get(this.state.user.cart.products[i], 'product.vendor', undefined));

					if (get(this.state.user.cart.products[i], 'product.vendor', undefined) == history[j]) {
						flag = false;
						break
					}
				}
				if(history.length==0||flag){
					history.push(get(this.state.user.cart.products[i], 'product.vendor', undefined))
				
				}
				// console.log("tst", this.state.user.cart.products[i].product.vendor)


			
		}

		count = history.length

		this.setState({ cashOnDeliveryCount: count })

	}
	onChangePostal = (e, addressType) => {
		if (addressType === "shipping") {
			const user = Object.assign({}, this.state.user)
			user.shipping.zip = e.target.value
			this.setState({ user: user }, this.refreshShipping)
		} else {
			const user_billing = Object.assign({}, this.state.user)
			user_billing.billing.zip = e.target.value
			this.setState({ user: user_billing }, this.refreshShipping)
		}
	}
	handleCountryChange = (objectType, countryObject) => {
		//console.log('handleCountryChange',countryObject )
		
		var _notificationSystem = this.refs.notificationSystem;

		const user = Object.assign({}, this.state.user)
		
		user[objectType].country = countryObject.Name
		user[objectType].country_code = countryObject.Code
		user[objectType].city = null
		this.getCities(countryObject.Code, objectType)
		this.setState({ user: user })
	}
	handleCityChange = (objectType, city) => {
		if (!city){
			return;
		}
		const user = Object.assign({}, this.state.user)
		user[objectType].city = city
		this.refreshShipping()
		this.setState({ user: user })
	}

	setPromocode = (promocode) => {
		let discount = 0;
		let current_total = this.props.location.state.totalPrice;
		if(promocode === 'BM20'){
			discount = current_total*.20;
			discount = discount.toFixed(2);
			current_total = current_total - Number(discount);
		}
		this.setState({
			cc_promocode: promocode,
			cc_discount: discount,
			total_after_discount: current_total
		}, () => {
			console.log(this.state.total_after_discount);
		});
	}

	render() {

		if(!localStorage.getItem("token")){
			this.props.history.push('/login')
		}
		if (!this.props.location.state) {
			this.props.history.push({ pathname: '/cart' })
			return (<div />)
		}
		const icons = {
			finish: <svg style={{ fill: "#4294e5", height: 12 }} xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z" /></svg>
		}
		if (Object.keys(this.state.user).length === 0)
			return (<div />)
		return (
			<div>
				<NotificationSystem ref="notificationSystem"  />

				<Modal
					className="shipping-modal"
					onHide={() => { this.setState({ termsModal: false }) }}
					show={this.state.termsModal}
					bsSize="large"
					aria-labelledby="contained-modal-title-lg"
				>
					<Modal.Header closeButton>
						<Modal.Title id="contained-modal-title-lg">Terms and Conditions</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<p>I hereby confirm that I have read and understood this Privacy Policy. By submitting data to BabMakkah or its agent(s) or by my using of BabMakkah’s website, applications or services, I voluntarily and expressly consent to BabMakkah’s collection, use, sharing, processing and retention of my data in the manner set out in this Privacy Policy.</p>
					</Modal.Body>
					<Modal.Footer>
						<Button onClick={() => this.setState({ termsModal: false })}>Close</Button>
					</Modal.Footer>
				</Modal>
				<Subscribe to={[CartContainer]}>
					{cart => { this.cart = cart; return ('') }}
				</Subscribe>
				<ShopCategoryInside />
				<div className="checkout">
					<Grid>
						<Row>
							<Col md={8}>
								<div className="checkout-content">
									<span className="checkout_secure">
										<h3>
											<i class="material-icons">
												security
                        			</i> {this.props.strings.checkout}
										</h3>
									</span>
									<div className="checkout-content__stepper">

										<Steps icons={icons} labelPlacement="vertical" current={1}>
											<Step title={this.props.strings.order} />
											<Step title={this.props.strings.shippingAndPayment} />
											<Step title={this.props.strings.done} />
										</Steps>									</div>
									<div className="section-title__holder">
										<div className="section-title">
											<h3>{this.props.strings.paymentType}*</h3>
										</div>
									</div>
									<Row>
										<Col md={6}>
											<FormGroup>
												<span className="payment-method">
													<Radio checked={this.state.method === 'online'} onClick={() => this.setState({ method: 'online' }, this.refreshShipping)} name="radioGroup" inline>
														{this.props.strings.onlinePayment}
													</Radio>
												</span>
												<span className="payment-method">
													<Radio checked={this.state.method === 'offline'} onClick={() => this.setState({ method: 'offline' }, this.refreshShipping)} name="radioGroup" inline>
														{this.props.strings.cashOnDelivery}
													</Radio>
												</span>
											</FormGroup>
										</Col>
									</Row>
									<div className="section-title__holder">
										<div className="section-title">
											<h3>{this.props.strings.personalInfo}</h3>
										</div>
									</div>
									<Row>
										<Col md={6}>
											<FormGroup controlId="formValidationNull">
												<ControlLabel>{this.props.strings.firstName}*</ControlLabel>
												<FormControl type="text" name="cc_first_name" onChange={(e) => { this.setState({ cc_first_name: e.target.value }) }} placeholder={this.props.strings.firstName} value={this.state.cc_first_name} />
											</FormGroup>
										</Col>
										<Col md={6}>
											<FormGroup controlId="formValidationNull">
												<ControlLabel>{this.props.strings.lastName}*</ControlLabel>
												<FormControl type="text" name="cc_last_name" onChange={(e) => { this.setState({ cc_last_name: e.target.value }) }} placeholder={this.props.strings.lastName} value={this.state.cc_last_name} />
											</FormGroup>
										</Col>
										<Col md={6}>
											<FormGroup controlId="formValidationNull">
												<ControlLabel>{this.props.strings.phoneNumber}*</ControlLabel>
												<ReactPhoneInput value={this.state.phone_number} onChange={this.handlePhoneChange} defaultCountry={'sa'} />
											</FormGroup>
										</Col>
										<Col md={6}>
											<FormGroup controlId="formValidationNull">
												<ControlLabel>{this.props.strings.email}*</ControlLabel>
												<FormControl type="email" name="email" onChange={(e) => { this.setState({ email: e.target.value }) }} placeholder={this.props.strings.email} value={this.state.email} />
											</FormGroup>
										</Col>
									</Row>




									<React.Fragment>
										<div className="section-title__holder">
											<div className="section-title">
												<h3>{this.props.strings.shippingAddress}</h3>
											</div>
										</div>
										<Row>
											{this.state.userInfos["shipping"].map((attribute, key) => (
												<Col md={6}>
													<FormGroup controlId={"shipping-" + attribute}>
														<ControlLabel>{this.props.strings[attribute]} *</ControlLabel>
														<FormControl onChange={(e) => this.onChangetext(e, "shipping", attribute)} defaultValue={attribute === 'phone' ? get(this.state.user, ["shipping", 'phone'], get(this.state.phone_number)) : get(this.state.user, ["shipping", attribute])} type="text" placeholder={this.props.strings[attribute]} />
													</FormGroup>
												</Col>
											))}
											<Col md={6}>
												<FormGroup controlId={"postal-code-shipping"}>
													<ControlLabel>{this.props.strings.postal_code_shipping}*</ControlLabel>
													<FormControl onChange={(e) => this.onChangePostal(e, "shipping")}
														value={this.state.user.shipping.zip}
													/>
												</FormGroup>
											</Col>
											<Col md={6}>
												<FormGroup>
													<ControlLabel>{this.props.strings.country}*</ControlLabel>
													<Select
														clearable={false}
														placeholder="Select your country"
														value={this.state.countries[findIndex(this.state.countries, { label: this.state.user.shipping.country })]}
														isLoading={this.state.user.shipping.country && this.state.countries.length === 0}
														onChange={(countryObject) => this.handleCountryChange('shipping', countryObject.value)}
														options={this.state.countries}
													/>
												</FormGroup>
											</Col>

											<Col md={6}>
												<FormGroup>
													<ControlLabel>{this.props.strings.city}*</ControlLabel>
													<Select
														clearable={false}
														placeholder="Select a city"
														value={this.state.cities.shipping[findIndex(this.state.cities.shipping, { value: this.state.user.shipping.city, label: this.state.user.shipping.city })]}
														isLoading={this.state.user.shipping.country && this.state.cities.shipping.length === 0}
														onChange={(cityObject) => this.handleCityChange('shipping', cityObject.value)}
														options={this.state.cities.shipping}
													/>
												</FormGroup>
											</Col>

										</Row>
									</React.Fragment>
									<Checkbox className="checkout-checkbox" checked={this.state.sameAsBilling} onClick={(e) => this.setState({ sameAsBilling: e.target.checked })}>{this.props.strings.billingEqualShipping}</Checkbox>
									{
										this.state.sameAsBilling ? (null) : (
											<React.Fragment>
												<div className="section-title__holder">
													<div className="section-title">
														<h3>{this.props.strings.billingAddress}</h3>
													</div>
												</div>

												<Row>
													{this.state.userInfos["billing"].map((attribute, key) => (
														<Col md={6}>
															<FormGroup controlId={"billing-" + attribute}>
																<ControlLabel>{this.props.strings[attribute]}* </ControlLabel>
																<FormControl onChange={(e) => this.onChangetext(e, "billing", attribute)} defaultValue={attribute === 'phone' ? get(this.state.user, ["billing", 'phone'], get(this.state.phone_number)) : get(this.state.user, ["billing", attribute])} type="text" placeholder={this.props.strings[attribute]} />
															</FormGroup>
														</Col>
													))}
													<Col md={6}>
														<FormGroup controlId={"postal-code-" + "billing"}>
															<ControlLabel>{this.props.strings.postal_code_billing}*</ControlLabel>
															<FormControl onChange={(e) => this.onChangePostal(e, "billing")}
																value={this.state.user.billing.zip}
															/>
														</FormGroup>
													</Col>
													<Col md={6}>
														<FormGroup>
															<ControlLabel>{this.props.strings.country}*</ControlLabel>
															<Select
																clearable={false}
																placeholder="Select your country"
																value={this.state.countries[findIndex(this.state.countries, { label: this.state.user.billing.country })]}
																isLoading={this.state.user.billing.country && this.state.countries.length === 0}
																onChange={(countryObject) => this.handleCountryChange('billing', countryObject.value)}
																options={this.state.countries}
															/>
														</FormGroup>
													</Col>

													<Col md={6}>
														<FormGroup>
															<ControlLabel>{this.props.strings.city}*</ControlLabel>
															<Select
																clearable={false}
																placeholder="Select a city"
																value={this.state.cities.billing[findIndex(this.state.cities.billing, { value: this.state.user.billing.city, label: this.state.user.billing.city })]}
																isLoading={this.state.user.billing.country && this.state.cities.billing.length === 0}
																onChange={(cityObject) => this.handleCityChange('billing', cityObject.value)}
																options={this.state.cities.billing}
															/>
														</FormGroup>
													</Col>
												</Row>
											</React.Fragment>
										)
									}

									<FormGroup>
										<div style={{ display: 'inline-flex' }}>
											<Checkbox className="checkout-checkbox" value={this.state.accept} onChange={(e) => { this.setState({ accept: (e.target.value === "on" ? ('off') : ('on')) }) }}>
												<p className="terms">
													{this.props.strings.iaccept}
												</p>
											</Checkbox>
											<span onClick={() => { this.setState({ termsModal: true }) }} className="terms-link">{this.props.strings.conditions}</span>
										</div>
									</FormGroup>
									<Button onClick={this.saveAddress} className="checkout-saveaddress__btn">{this.props.strings.saveAddress}</Button>
									<Col md={12} className="payment-section">
										<div className="section-title__holder">
											<div className="section-title">
												<h3>Payment method</h3>
											</div>
										</div>
										<div className="checkout_choose">
											<h4> How would you like to pay  <em>{this.props.location.state.totalPrice} SAR</em> </h4>
										</div>
										<div className="checkout-payment">
											<span className="checkout-method">
												<h3><i class="material-icons">
													check_circle
                           				 </i>
													Cash on delivery (COD)
                        					</h3>
											</span>
											<span className="checkout__tax">
												<h3>+ $1 </h3>
											</span>
										</div>
										<Button onClick={this.placeOrder.bind(this)} className="checkout-Order__btn main-btn">Place Order</Button>
									</Col>
								</div>
							</Col>
							<Col style={{ top: 20 }} md={4}>
								<div className="checkout-cart">
									<div className="checkout-holder">
										<span className="shoping-cart__title"> {this.props.strings.cart} </span>
										<span className="shoping-cart__edit"> <a href="/cart"> {this.props.strings.edit} </a> </span>
										{this.props.location.state.products.map((product, index) => (
											<React.Fragment>
												<ProductItem checkout product={product.product} />
												<div className="ProductItem-qty">
													<h3><em> {this.props.strings.qty + product.quantity} </em> </h3>
												</div>
											</React.Fragment>
										))}
										<div className="checkout-sum">
											<div className="checkout-sum__text">
												<h3>{this.props.strings.Items}</h3>
											</div>
											<div className="checkout-sum__count">
												<h3> {  this.props.location.state.totalPrice  - this.state.cc_discount + ' SAR'} </h3>
											</div>
											{(this.state.cc_discount > 0)? (
												<div>
													<div className="checkout-sum__text">
														<h3>{this.props.strings.discount}</h3>
													</div>
													<div className="checkout-sum__count">
														<h3> {this.state.cc_discount + ' SAR'} </h3>
													</div>
												</div>
												) : (
													<div></div>
													)
											}
											<div className="checkout-sum__text">
												<h3>{this.props.strings.shipping}</h3>
											</div>
											<div className="checkout-sum__count">
											{console.log('shippingTotal', this.state.shippingTotal)}
												{/* <h3> {this.state.shippingError ? this.state.shippingError : this.state.shippingTotal ? (toPrice(this.state.shippingTotal) + ' SAR') : (this.props.strings.calculating)} </h3> */}
												{/* <h3> {this.state.shippingError ? this.state.shippingError : this.props.location.state.totalPrice > 200 ? 'Free Shipping ' : (toPrice(this.state.shippingTotal) + ' SAR') } </h3> */}
											 {console.log('country12', this.state.user.shipping.country)}
												<h3> {this.state.shippingError ? this.state.shippingError :  this.props.location.state.totalPrice > 200 && this.state.user.shipping.country === 'Saudi Arabia' ? 'Free Shipping ' :  this.state.shippingTotal ? (toPrice(this.state.shippingTotal) + ' SAR') : (this.props.strings.calculating) }</h3>
											</div>
											<br />

											{this.state.method === "offline" ?

												<div className="checkout-sum__text">

													<h3>{this.props.strings.cashOnDelivery}</h3>

												</div>
												: ""
											}
											{this.state.method === "offline" ?
												<div className="checkout-sum__count">
													<h3>{(toPrice(cashOnDelivery * this.state.cashOnDeliveryCount) + ' SAR')}</h3>
												</div>
												: ""
											}






										</div>

										<div className="checkout-grand__total">
											<div className="grand-total__text">
												<h3> {this.props.strings.total} </h3>
											</div>
											<div className="grand-total__count">
											{(this.state.cc_discount > 0) ? (
												<h3> {this.state.shippingError ? '-' : this.state.shippingTotal ? (toPrice(this.state.total_after_discount * 1 + this.state.shippingTotal * 1 + (this.state.method === "offline" ? (cashOnDelivery * this.state.cashOnDeliveryCount) : 0)) + " SAR") : (this.props.strings.calculating)} </h3>
												):
											(
												<h3> {this.state.shippingError ? '-' : this.props.location.state.totalPrice > 200 && this.state.user.shipping.country === 'Saudi Arabia' ? (toPrice(this.props.location.state.totalPrice * 1  + (this.state.method === "offline" ? (cashOnDelivery * this.state.cashOnDeliveryCount) : 0) ) + " SAR") : (toPrice(this.props.location.state.totalPrice * 1 + this.state.shippingTotal * 1 + (this.state.method === "offline" ? (cashOnDelivery * this.state.cashOnDeliveryCount) : 0)) + " SAR")} </h3>
												// <h3> {this.props.location.state.totalPrice > 200  ? (toPrice(this.props.location.state.totalPrice * 1  + (this.state.method === "offline" ? (cashOnDelivery * this.state.cashOnDeliveryCount) : 0)) + " SAR") : (toPrice(this.props.location.state.totalPrice * 1 + this.state.shippingTotal * 1 + (this.state.method === "offline" ? (cashOnDelivery * this.state.cashOnDeliveryCount) : 0)) + " SAR")} </h3>
											//	<h3> {this.state.shippingError ? '-' : this.state.shippingTotal  ? (toPrice(this.props.location.state.totalPrice * 1 + this.state.shippingTotal * 1 + (this.state.method === "offline" ? (cashOnDelivery * this.state.cashOnDeliveryCount) : 0) ) + " SAR"):(toPrice(this.props.location.state.totalPrice * 1  + (this.state.method === "offline" ? (cashOnDelivery * this.state.cashOnDeliveryCount) : 0)) + " SAR") : toPrice(this.props.location.state.totalPrice * 1 + this.state.shippingTotal * 1 + (this.state.method === "offline" ? (cashOnDelivery * this.state.cashOnDeliveryCount) : 0)) + " SAR"}</h3>
											)}
												
											</div>
										</div>
										<div className="checkout-promocode__input">
											<FormGroup controlId="formValidationNull">
												<ControlLabel>{this.props.strings.promocode}</ControlLabel>
												<FormControl type="text" name="cc_promocode" onChange={(e) => { this.setPromocode(e.target.value)}} placeholder={this.props.strings.promocode} value={this.state.cc_promocode} />
											</FormGroup>
										</div>
										<Button disabled={!!this.state.shippingError} onClick={this.placeOrder.bind(this)} className="checkout-Order__btn main-btn">{this.props.strings.done}</Button>
									</div>
								</div>
							</Col>
						</Row>
					</Grid>
				</div>
			</div>
		);
	}
}
export default Checkout;
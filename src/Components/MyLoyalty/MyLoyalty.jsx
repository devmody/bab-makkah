import React, { Component } from 'react';
import { Grid, Row, Col, Table } from 'react-bootstrap'
import { getObjectWithToken } from '../../services/CommonServices'
import AccountSidebar from '../AccountSidebar/AccountSidebar';
import config from '../../config';

class MyLoyalty extends Component {
    state = {
        user: {
            loyaltyHistory: []
        }
    }
    componentDidMount() {
        getObjectWithToken('users/profile')
            .then((response) => {
                this.setState({ user: response.data })
            })
    }
    render() {

        return (
            <div className="account-wishlist">
            <Grid>
                <Row>
                    <Col md={3}>
                        <AccountSidebar strings={this.props.strings} />
                    </Col>
                    <Col md={9}>
                        <h2>{this.props.strings.ComingSoon}</h2>
                    </Col>
                </Row>
            </Grid>
        </div>
        )
        return (
            <div className="account-wishlist">
                <Grid>
                    <Row>
                        <Col md={3}>
                            <AccountSidebar strings={this.props.strings} />
                        </Col>
                        <Col md={9}>
                            <h5>{this.props.strings.currentlyHave}</h5>
                            <h2>{this.state.user.loyalityPoints} {this.props.strings.points}</h2>
                            <h5>
                                {this.props.strings.useLater}
                            </h5>
                            <h2>{this.props.strings.code}: {this.state.user.loyalityId}</h2>
                            {this.state.user.loyaltyHistory.length !== 0 ? (
                                <Table>
                                    <thead>
                                        <tr>
                                            <th>
                                                {this.props.strings.amount}
                                </th>
                                            <th>
                                                {this.props.strings.orderDate}
                                </th>
                                            <th>
                                                {this.props.strings.reason}
                                </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.user.loyaltyHistory.map((log) => (
                                            <tr>
                                                <td>
                                                    {log.amount}
                                                </td>
                                                <td>
                                                    {new Date(log.date).toLocaleDateString()} , {new Date(log.date).getHours()}:{new Date(log.date).getMinutes()}
                                                </td>
                                                <td>
                                                    {!log.reason ? ('Order ' + log.order.orderId) : (config.reasons[log.reason])}
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </Table>
                            ) : (
                                    ''
                                )}
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default MyLoyalty;
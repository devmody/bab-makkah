import React from "react";
import { Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, ControlLabel, FormGroup, Navbar, Carousel, Tabs, Tab, FormControl } from 'react-bootstrap'
import ShopCategoryInside from "../ShopCategoryInside/ShopCategoryInside";
import StarRatingComponent from "react-star-rating-component";
import { updateObjectWithToken, calculateRatingAverage, toPrice, getObjectWithToken, getSingleObject, toCloudinary } from "../../services/CommonServices";
import { get } from 'lodash'
import { Link, withRouter } from 'react-router-dom'


class ViewProductsOrder extends React.Component {
    constructor(props) {
        super(props)
        this.onStarClick = this.onStarClick.bind(this)
        this.getPrice = this.getPrice.bind(this)
        //this.getTotal = this.getTotal.bind(this)
        this.state = {
            products: [],
            productsPrices: [],
            price: "",
            total: 0,
            rating: {},
            userId: '',
            order: ''
        }
    }
    onStarClick(nextValue, prevValue, name, orderProduct) {
        let products = this.state.products;
        let reviews;
        for (let i = 0; i < products.length; i++) {
            reviews = products[i].product.reviews
            for (let j = 0; j < reviews.length; j++) {
                if (products[i].product._id === orderProduct) {
                    reviews[j].rating = nextValue;
                }
            }
        }
        this.setState({ products: products });
        updateObjectWithToken("product/review", {
            _id: orderProduct,
            rating: nextValue
        }).then(response => {
            if (response.data.error) {
                console.log(response.data.error)
            } else {
                this.componentDidMount();

            }
        });
    }
    getPrice(product, sizeIndex, productIndex) {
        let prices = this.state.productsPrices.slice(0)
        if (prices[productIndex] !== product.sizes[sizeIndex].price) {
            prices[productIndex] = product.sizes[sizeIndex].price
            this.setState({ productsPrices: prices })
        }
        return product.sizes[sizeIndex].price.toFixed(2);
    }
    calculateDiscount(orderTotal , shippingCost) {
        let totalPrices = 0;
        for (let i = 0; i < this.state.products.length; i++) {
            totalPrices += this.state.products[i].product.sizes[this.state.products[i].size].price * this.state.products[i].quantity
        }
        totalPrices = totalPrices 
        return totalPrices - (orderTotal- shippingCost -18);
        //this.setState({ total: totalPrices })
    }

    
    calulateSubTotalAmount(total, shippingCost) {
        let subTotal = total - shippingCost - 18
        return subTotal
    }

    componentDidMount() {
        window.scroll(0, 0)
        var context = this
        getObjectWithToken('users/profile')
            .then((response) => {
                context.setState({
                    userId: response.data._id
                })
            })
        getSingleObject('order', this.props.match.params.id)
            .then((response) => {
              
            context.setState({ products: response.data.products,total: response.data.total,  order: response.data })
            })
    }
    componentDidUpdate() {
        window.scroll(0, 0)

    }


    render() {
        console.log('render');
        const product = this.state.products
        const order = this.state.order
        console.log('order', order);
        if (!localStorage.getItem("token")) {
            this.props.history.push("/login")
            return (<div />)
        }
        const starRatingProps = {
            starCount: 5,
            renderStarIcon: () => (
                <span>
                    {" "}
                    <i className="fa fa-star" />{" "}
                </span>
            ),
            emptyStarColor: "#dddddd",
        };
        return (
            <div>
                <ShopCategoryInside />
                <div className="cart checkout">
                    <Grid>
                        <Col md={9}>
                            {
                                this.state.products.map((product, index) => (
                                    product.product ?
                                        <div className="cart-border">
                                            <Row>
                                                <Col md={4}>
                                                    <div className="view-product__image">
                                                        <Link to={"/product/" + product.product._id}>
                                                            <img src={toCloudinary(product.product.images[0], null, 241) || require("../../img/Bab Makkah logo.png")}
                                                                onError={e => {
                                                                    e.target.src = require("../../img/Bab Makkah logo.png");
                                                                }}
                                                            />
                                                        </Link>
                                                    </div>
                                                </Col>
                                                <StarRatingComponent
                                                    name="rate1"
                                                    {...starRatingProps}
                                                    value={get(product.product.reviews.find((review) => {

                                                        return review.reviewer === this.state.userId
                                                    }), 'rating', '')}
                                                    onStarClick={(nextValue, prevValue, name) => this.onStarClick(nextValue, prevValue, name, product.product._id)}
                                                />
                                                <Col md={6}>
                                                    <div className="view_products-details">
                                                        <div className="cart-category">
                                                            <h4> {product.product.type.name ? product.product.type.name[sessionStorage.getItem('lang')] : 'SmartPhones'} </h4>
                                                        </div>
                                                        <div className="cart-item">
                                                            <Link to={"/product/" + product.product._id}>
                                                                <h4>

                                                                    {product.product.name ? product.product.name[sessionStorage.getItem('lang')] : 'Iphone'}

                                                                </h4>
                                                            </Link>
                                                        </div>
                                                        <table>
                                                            <colgroup>
                                                                <col width="50%" />
                                                            </colgroup>
                                                            <tbody>
                                                                <td>
                                                                    <tr>
                                                                        <td>{this.props.strings.size}</td><td>{product.product.sizes[product.size] ? product.product.sizes[product.size].name[sessionStorage.getItem('lang')] : 'Large'}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>{this.props.strings.color}</td><td>{product.product.sizes[product.size] && product.product.sizes[product.size].colors && product.product.sizes[product.size].colors.name ? product.product.sizes[product.size].colors.name[sessionStorage.getItem('lang')] : '-'}</td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>{this.props.strings.price}</td><td className="cart__total-price">{this.getPrice(product.product, product.size, index)} SAR</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> {this.props.strings.quantity} </td><td>{product.quantity ? product.quantity : "-"}</td>
                                                                    </tr>
                                                                </td>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </Col>
                                            </Row>
                                        </div>
                                        : "PRODUCT DELETED!"))}
                        </Col>
                        <Col md={3}>
                            <div className="cart__total">
                                {
                                    this.state.products.map((product, index) => (
                                        order.shippingCost && product.product ?
                                            <React.Fragment>
                                                <h5>{this.props.strings.sub}</h5>
                                                <h3>{toPrice(this.calulateSubTotalAmount(order.total, order.shippingCost))} SAR </h3>
                                                <h5>{this.props.strings.shipping}</h5>
                                                <h3>{toPrice(order.shippingCost) + " SAR"} </h3>
                                                <h5>Discount</h5>
                                                <h3>{toPrice(this.calculateDiscount(order.total, order.shippingCost)) + " SAR"} </h3>
                                                {
                                                    this.state.order.paymentReference === "" ?
                                                        <div>
                                                            <h5>{this.props.strings.cashOnDelivery}</h5>
                                                            <h3>{toPrice(18) + " SAR"} </h3>
                                                        </div>
                                                        : ""
                                                }
                                                <h5>{this.props.strings.total}</h5>
                                                <h3>{toPrice(this.state.total)} SAR  </h3>
                                            </React.Fragment>




                                            : ""
                                    ))

                                }
                            </div>
                        </Col>
                    </Grid>
                </div>
            </div>
        );
    }
}
export default withRouter(ViewProductsOrder);
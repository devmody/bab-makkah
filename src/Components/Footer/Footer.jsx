import React from "react";
import { Link } from 'react-router-dom'
import { Row, Col } from 'react-bootstrap'
import MasterCard from './mastercard.png'
import Visa from './visa.png'
import Mada from './mada-logo.jpg'
// import Sadad from './sadad-logo.jpg'

class Footer extends React.Component {
    render() {
        return (

            <div className="footer">
                <div className="container">
                    <Row className="align-item">
                        <Col md={3}>
                            <img style={{
                                width: 160,
                                height: 147
                            }} src={require("../../img/logo.png")} />
                        </Col>
                        <Col md={5}>
                            <div className="footer-Call">
                                <div className="footer-Call-img">
                                    <img src={require("../../img/callus.png")} />
                                </div>
                                <div className="footer-Call-caption">
                                    <p>{this.props.strings.questions}</p>
                                    <h3> <a href="tel:+966 533 571 555">&#x200E;+966 533 571 555</a> </h3>
                                    <h3 className="babmakkah-email" > <a href="mailto:info@babmakkah.com">info@babmakkah.com</a> </h3>
                                </div>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div className="footer-Call-Adress">
                                {/* <h3><Link>Are you a Vendor?</Link> </h3> */}
                                {/* <ul> */}
                                {/* <li className="footer-Call-Adress___Address"> */}
                                <span className="footer-value"><h4><Link to="/vendor-register">{this.props.strings.areVendor}</Link></h4></span>
                                {/* </li> */}
                                {/* <li>
                                        <span className="special-color">{this.props.strings.phone} </span><span className="footer-value"><a href="tel:+966 533 571 555">&#x200E;+966 533 571 555</a></span>
                                    </li>
                                    <li>
                                        <span className="special-color"> {this.props.strings.email} </span><span className="footer-value"><a href="mailto:info@babmakkah.com">info@babmakkah.com</a></span>
                                    </li>
                                </ul> */}
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>
                            <hr />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={3}>
                            <div className="section-title__holder">
                                <div className="section-title">
                                    <h3>{this.props.strings.myaccount}</h3>
                                </div>
                            </div>
                            <div className="footer-link">
                                <Link to="/account">{this.props.strings.myaccount}</Link>
                            </div>
                            <div className="footer-link">
                                <Link to="/checkout">{this.props.strings.Checkout}</Link>
                            </div>
                            <div className="footer-link">
                                <Link to="/cart">{this.props.strings.Shoppingcart}</Link>
                            </div>
                            <div className="footer-link">
                                <Link to="/account-wishlist">{this.props.strings.wishlist}</Link>
                            </div>
                            <div className="footer-link">
                                <Link to="/account-orders">{this.props.strings.orderHistory}</Link>
                            </div>
                        </Col>
                        <Col md={3}>
                            <div className="section-title__holder">
                                <div className="section-title">
                                    <h3>{this.props.strings.usefullinks}</h3>
                                </div>
                            </div>
                            <div className="footer-link">
                                <Link to="/aboutus">{this.props.strings.aboutus}</Link>
                            </div>
                            <div className="footer-link">
                                <Link to="/terms">{this.props.strings.conditions}</Link>
                            </div>
                            <div className="footer-link">
                                <Link to="/privacy-policy">{this.props.strings.terms}</Link>
                            </div>
                            <div className="footer-link">
                                <Link to="/return-policy">{this.props.strings.returnpolicy}</Link>
                            </div>
                            <div className="footer-link">
                                <Link to="/cancellation-policy">{this.props.strings.cancellationpolicy}</Link>
                            </div>
                            <div className="footer-link">
                                <Link to="/estimated-delivery">{this.props.strings.deliverytime}</Link>
                            </div>
                            <div className="footer-link">
                                <Link to="/contact-form">{this.props.strings.contactus}</Link>
                            </div>
                        </Col>
                        <Col md={3}>
                            <div className="section-title__holder">
                                <div className="section-title">
                                    <h3>{this.props.strings.featuredcategories}</h3>
                                </div>
                            </div>
                            <div className="footer-link">
                                <Link to="/shop?category=5bf2bfc569049645c02991e7">{this.props.strings.pottery}</Link>
                            </div>
                            <div className="footer-link">
                                <Link to="/shop?category=5bf2bed369049645c02991de">{this.props.strings.leather}</Link>
                            </div>
                            <div className="footer-link">
                                <Link to="/shop?category=5bed353a7070842a21530938">{this.props.strings.subha}</Link>
                            </div>
                            <div className="footer-link">
                                <Link to="/shop?category=5bf2bf6f69049645c02991e4">{this.props.strings.travelproducts}</Link>
                            </div>
                            <div className="footer-link">
                                <Link to="/shop?category=5bf2bf0069049645c02991e0">{this.props.strings.carpets}</Link>
                            </div>
                        </Col>
                        <Col md={3}>
                            <div className='images-logo'>
                                <Row>
                                    <Col md={6}>
                                        <img id="mc-image" src={MasterCard} />
                                    </Col>
                                    <Col md={6}>
                                        <img id="visa-image" src={Visa} />
                                    </Col>

                                </Row>
                                <Row>
                                    <Col md={6}>
                                        <img id="mada-image" src={Mada} />
                                    </Col>
                                    {/* <Col md={6}>
                                        <img id="sadad-image" src={Sadad} />
                                    </Col> */}
                                </Row>

                            </div>
                        </Col>
                    </Row>
                </div>
                <div className="footer-bottom">
                    © {new Date().getFullYear()} BABMAKKAH. All rights reserved
                </div>
            </div>
        );
    }
}

export default Footer;

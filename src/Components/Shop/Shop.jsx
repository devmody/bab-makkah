import React from "react";
import { Col } from 'react-bootstrap'
import ShopFilter from "../ShopFilter/ShopFilter";
import GridProduct from "../GridProduct/GridProduct";
import ShopSort from "../ShopSort/ShopSort";
import { getAllObjects } from "../../services/CommonServices";
import { withRouter } from 'react-router-dom'
import queryString from 'query-string'
import { Row } from 'react-bootstrap'
import { isEqual } from 'lodash/lang'
import './Shop.css'
class Shop extends React.Component {
  state = {
    categories: [],
    types: [],
    vendors: [],
    products: [],
    brands: [],
    currentPage: 0,
    limit: 12,
    totalCount: 0
  }
  componentDidMount() {
    let query = queryString.parse(this.props.location.search)
    if (!query.page) {
      query.page = 0;
      this.props.history.push({
        pathname: 'shop',
        search: queryString.stringify(query)
      })
      return;
    }
    this.componentWillReceiveProps(this.props)
  }
  componentWillReceiveProps(newProps) {
    window.scroll(0, 300)
    const query = queryString.parse(newProps.location.search)
    this.setState({ ready: false })
    getAllObjects('category',{lang:sessionStorage.getItem("lang")})
      .then((response) => {
        this.setState({ categories: response.data })
      })
    getAllObjects('type', { category: query.category,lang:sessionStorage.getItem("lang") })
      .then((response) => {
        this.setState({ types: response.data })
      })
      getAllObjects('brand', {lang:sessionStorage.getItem("lang") })
      .then((response) => {
        this.setState({ brands: response.data })
      })
    getAllObjects('vendors', {lang:sessionStorage.getItem("lang")})
      .then((response) => {
        this.setState({ vendors: response.data })
      })
    query.sortBy = queryString.parse(query.sortBy)
    if (isEqual(query, {})) {
      delete query.sortBy
    }
    getAllObjects('product/search-filtered', { ...query, limit: this.state.limit })
      .then((response) => {
        this.setState({ products: response.data.products, totalCount: response.data.totalCount, ready: true })
      })
  }
  onPageChange(page) {
    let query = queryString.parse(this.props.location.search)
    query.page = page
    this.props.history.push({
      pathname: 'shop',
      search: queryString.stringify(query)
    })
  }
  render() {
    const query = queryString.parse(this.props.location.search)
    return (
      <div>
        {/* <ShopCategoryInside /> */}

        {/* <div className="breadcrumb-holder">
          <Breadcrumbs />
        </div> */}
        <div className="shop">
          <div className="container">
            <Row>
              <Col style={{
                background: 'white'
              }} md={3}>
                <ShopFilter
                  {...this.props}
                  categories={this.state.categories}
                  types={this.state.types}
                  vendors={this.state.vendors}
                  brands={this.state.brands}
                />
              </Col>
              <Col className="grid-col" style={{ background: 'white' }} md={9}>
                <ShopSort
                  strings={this.props.strings}
                  currentPage={query.page}
                  limit={this.state.limit}
                  totalCount={this.state.totalCount}
                />
                <GridProduct
                  ready={this.state.ready}
                  strings={this.props.strings}
                  currentPage={query.page}
                  products={this.state.products}
                  limit={this.state.limit}
                  onPageChange={this.onPageChange.bind(this)}
                  totalCount={this.state.totalCount}
                />
              </Col>
            </Row>
          </div>
        </div>
      </div>
    );
  }
}
export default withRouter(Shop);
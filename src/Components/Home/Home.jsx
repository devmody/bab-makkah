import React from "react";
import HomeSlider from "../HomeSlider/HomeSlider";
import HomeBanner from "../HomeBanner/HomeBanner";
import { Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, NavItem, Navbar, Carousel } from 'react-bootstrap'
import DeliveryProcess from "../DeliveryProcess/DeliveryProcess";
import PhoneCategory from "../PhoneCategory/PhoneCategory";
import FeatuedCategoris from "../FeatuedCategoris/FeatuedCategoris";
import BestSeller from "../BestSeller/BestSeller";
import HomeAds from "../HomeAds/HomeAds";
import ShopCategory from '../ShopCategory/ShopCategory'
import AccountSidebar from "../AccountSidebar/AccountSidebar";
import Confetti from 'react-dom-confetti';
import ReactCountdownClock from 'react-countdown-clock';
import { getAllObjects } from "../../services/CommonServices";
class Home extends React.Component {

    constructor() {
        super()
        this.state = {
            confetti: false,
            overlay: "flex",
            overlayBackground: "rgba(0,0,0,0.9)",
            paused: true,
            timerAlpha: 0,
            btnDisplay: null,
            categories:[],
            types:[]
        }
    }

    launchWebsite = () => {
        this.setState({
            paused: false,
            timerAlpha: 0.9
        });
    }

    explode = () => {
        this.setState({
            confetti: true,
            btnDisplay: 'none',
            overlayBackground: "transparent"
        });
        setInterval(() => {
            this.setState({
                // confetti: !this.state.confetti
            })
        }, 3000)
        setTimeout(() => {
            this.setState({
                overlay: "none"
            });
        }, 3000)
    }
    componentDidMount() {
        
        window.scroll(0, 0)
        getAllObjects('cms')
        .then((response)=>{
            this.setState(Object.assign(this.state,response.data))
            
        })
        getAllObjects('type/featured')
        .then((response)=>{
            this.setState({types:response.data})
        })
        this.props.refreshFunction()
        
    }
    
    render() {
        const config = {
            angle: 90,
            spread: 171,
            startVelocity: 45,
            elementCount: 200,
            decay: 0.9
        };
        return (
            <div className="Home">
                {/* <ShopCategory {...this.props} /> */}
                {(this.props.launch) ?
                    (
                        <div className="home-overlay" style={{ display: this.state.overlay, background: this.state.overlayBackground }}>
                            <div className="btn-cont">
                                <ReactCountdownClock seconds={2}
                                    color="#fff"
                                    alpha={this.state.timerAlpha}
                                    size={300}
                                    paused={this.state.paused}
                                    onComplete={this.explode} />
                                <div className="confetti">
                                    <Confetti classname="confetti" active={this.state.confetti} config={config} style={{ display: "flex" }} />
                                </div>
                                <Button style={{ display: this.state.btnDisplay }} bsStyle="primary" onClick={this.launchWebsite}>إطلاق</Button>
                            </div>
                        </div>
                    ) : null}

                <div style={{background:'white'}} className="container">
                    {/* <ShopCategory/> */}
                    <HomeSlider bannerImage={this.state.homebanner} {...this.props} />
                    <HomeBanner />
                    {/*<DeliveryProcess/> */}
                    {
                        this.state.categories.map((category)=>(
                            <PhoneCategory category={category} />
                        ))
                    }
                </div>
                <FeatuedCategoris
                    types={this.state.types}
                    strings={this.props.strings} />
                <BestSeller
                    strings={this.props.strings} />
                <HomeAds bannerImage={this.state.bottombanner} strings={this.props.strings} />
            </div>
        );
    }
}

export default Home;

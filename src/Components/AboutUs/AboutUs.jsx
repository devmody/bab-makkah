import React from "react";
import './AboutUs.css';
import ReactPlayer from 'react-player';
import { Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, ControlLabel, FormGroup, Navbar, Carousel, Tabs, Tab, FormControl } from 'react-bootstrap';

class AboutUs extends React.Component {
    constructor(props) {
        super(props)
    }
    componentDidMount() {
        window.scroll(0, 0)
      }

    render() {
        
        return (
            <div className={sessionStorage.getItem('lang')==='en'?'Terms':'Terms-ar'}>
                <Grid>
                    <h3>{this.props.strings.whoWeAre}</h3>
                    <p> {this.props.strings.whoWeAreDesc}</p>
                
                    
                    <h4>{this.props.strings.BabMakkahGifts}</h4>
                    <p>{this.props.strings.BabMakkahGiftsDesc}</p>

                    <h4>{this.props.strings.WhatWeAspire} </h4>
                    <p> {this.props.strings.WhatWeAspireDesc}</p>
                    
                    
                    <div>
                        <ReactPlayer
                            url='https://vimeo.com/336347548'
                            className='react-player'
                            playing
                            className="video-aboutUs"
                            
                        />
                    </div>

                </Grid>
            </div>
        );
    }
}
export default AboutUs;
import React from "react";
import { Row, Col, Grid, Button } from 'react-bootstrap'
import AccountSidebar from "../AccountSidebar/AccountSidebar";
import AccountHeader from "../AccountHeader/AccountHeader";
import { updateObjectWithToken, getObjectWithToken } from "../../services/CommonServices";
import { Redirect } from 'react-router-dom'
import { toast, Flip } from 'react-toastify';
import { getAllObjects } from "../../services/CommonServices";
import queryString from 'query-string'
import CustomModal from '../../GenericComponents/Modal/Modal'
import axios from 'axios'
import config from '../../config'

class AccountSetting extends React.Component {
    toastSettings = {
        position: "top-right",
        autoClose: 5000,
        transition: Flip,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        className: "sub-toast"
    }
    constructor(props, context) {
        super(props, context);
        const showAccount = queryString.parse(this.props.location.search).new
        this.handleHide = this.handleHide.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.state = {
            showAccount: showAccount,
            showShipping: false,
            user: { shipping: {}, billing: {} },
            cities: {
                shipping: [],
                billing: []
            },
            countries: [],
            userAccount: {
                account: ["name", "email", "gender", "nationality"],
                shipping: ["address_1", "address_2", "area", "country", "city", "zip"],
                billing: ["address_1", "address_2", "area", "country", "city", "zip"],
            }
        };
    }
    handleHide(show) {
        this.setState({ [show]: false }, this.componentDidMount);
    }
    // getCities = (country, objectType) => {
    // 	const cities = Object.assign({}, this.state.cities)
    // 	cities[objectType] = []
    // 	this.setState({ cities: cities }, () => {
    //         alert(objectType)
    // 		getAllObjects('order/cities?country=' + country)
    // 			.then((response) => {
    // 				this.setState({ cities: Object.assign(this.state.cities, { [objectType]: response.data.map((c) => ({ value: c, label: c })) }) })
    // 			})
    // 			.catch((err) => {
    // 				console.log(err)
    // 			})
    // 	})
    // }
    componentDidMount() {
        getObjectWithToken('users/profile')
            .then((response) => {
                this.setState({
                    user: response.data,
                    currentEmail: response.data.email
                }, () => {
                    this.getCities(response.data.shipping.country_code, 'shipping')
                    this.getCities(response.data.billing.country_code, 'billing')
                })
            })
        getAllObjects('order/countries')
            .then((response) => {
                this.setState({ countries: response.data.map((c) => ({ value: c, label: c.Name })) })
            })
            .catch((err) => {
                console.log(err)
            })
    }
    handleChange(e, attr) {
        let user = Object.assign({}, this.state.user)
        if (attr) {
            if (!user[attr])
                user[attr] = {}
            user[attr][e.target.name] = e.target.value;
        }
        else
            user[e.target.name] = e.target.value;
        this.setState({ user: user })
    }
    handleChangeNationality(e) {
        let user = Object.assign({}, this.state.user)
        user.nationality = e.value;
        this.setState({ user: user })
    }

    getCities = (country, objectType) => {
        const cities = Object.assign({}, this.state.cities)
        cities[objectType] = []
        this.setState({ cities: cities }, () => {
            getAllObjects('order/cities?country=' + country)
                .then((response) => {
                    this.setState({ cities: Object.assign(this.state.cities, { [objectType]: response.data.map((c) => ({ value: c, label: c })) }) })
                })
                .catch((err) => {
                    console.log(err)
                })
        })
    }
    handleCountryChange = (e, objectType) => {
        let user = Object.assign({}, this.state.user)
        user[objectType].country = e.value.Name
        user[objectType].country_code = e.value.Code
        this.getCities(e.value.Code, objectType)
        this.setState({ user: user })
    }
    handleCityChange = (e, objectType) => {
        let user = Object.assign({}, this.state.user)
        user[objectType].city = e.value
        this.setState({ user: user })
    }
    handleSubmit(e, show) {
        e.preventDefault();
        if (this.state.currentEmail !== this.state.user.email) {
            window.emailVerification(this.state.user, this.state.user.email, axios, config.API_URL, (success) => {
                if (success) {
                    toast.success(this.props.strings.accountSuccess, this.toastSettings)
                    this.setState({ [show]: false })

                }
                else {
                    toast.error(this.props.strings.error, this.toastSettings)
                }
                this.componentDidMount()
            })
        } else {
            updateObjectWithToken('users/profile', this.state.user)
                .then(response => {
                    toast.success(this.props.strings.accountSuccess, this.toastSettings)
                    this.setState({ [show]: false })
                })
                .catch(err => {
                    toast.error(this.props.strings.error, this.toastSettings)
                })
        }

    }
    genderToString(gender) {
        if (gender + "" === "1") {
            return this.props.strings.male
        } else if (gender + "" === "0") {
            return this.props.strings.female
        }
        return null
    }
    render() {

        if (!localStorage.getItem("token")) {
            return <Redirect to="login" />
        }
        return (
            <div className="Account-setting">
                <AccountHeader user={this.state.user} componentDidMount={this.componentDidMount.bind(this)} />
                <Grid>
                    <Row>
                        <Col style={{ marginTop: 60 }} md={3}>
                            <AccountSidebar strings={this.props.strings} />
                        </Col>
                        <Col md={9}>
                            <div className="account-setting">
                                {Object.keys(this.state.userAccount).map((userInfo, index) => (

                                    <div className="account-setting__one">
                                        <div className="section-title__holder">
                                            <div className="section-title">
                                                <h3>{this.props.strings[userInfo]}</h3>
                                                <Button onClick={() => this.setState({ [userInfo]: true })} className="Account-Edit__btn main-btn">{this.props.strings.edit}</Button>
                                            </div>
                                            <table >
                                                <tbody>
                                                    {this.state.userAccount[userInfo].map((attr, key) => (
                                                    userInfo === 'shipping' || userInfo === 'billing' ?
                                                            <tr>
                                                        <td className="table_left_column">{this.props.strings[attr]}</td><td className="table_right_column">{this.state.user[userInfo] ? this.state.user[userInfo][attr] ? this.state.user[userInfo][attr] : 'N/A' : 'N/A'}</td>
                                                    </tr>
                                                    :
                                                    attr === 'gender' ?
                                                                <tr>
                                                        <td className="table_left_column">{this.props.strings[attr]}</td><td className="table_right_column">{this.genderToString(this.state.user[attr]) || 'N/A'}</td>
                                                    </tr>
                                                    :
                                                                <tr>
                                                        <td className="table_left_column">{this.props.strings[attr]}</td><td className="table_right_column">{this.state.user[attr] || 'N/A'}</td>
                                                    </tr>
                                                    ))}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                ))}
                            </div>
                            {console.log(this.state)}

                            <div className="modal-container">
                                <CustomModal handleHide={() => this.handleHide('account')} show={this.state.account}
                                    handleSubmit={(e) => this.handleSubmit(e, 'account')}
                                    handleChange={(e) => this.handleChange(e)} user={this.state.user}
                                    handleChangeNationality={this.handleChangeNationality.bind(this)}
                                    strings={this.props.strings} fields={["name", "email", "gender", "nationality"]} />
                                <CustomModal
                                    shippingOrBilling={'shipping'}
                                    handleHide={() => this.handleHide('shipping')}
                                    show={this.state.shipping}
                                    handleSubmit={(e) => this.handleSubmit(e, 'shipping')}
                                    handleChange={(e) => this.handleChange(e, 'shipping')}
                                    handleCountryChange={(e) => this.handleCountryChange(e, 'shipping')}
                                    handleCityChange={(e) => this.handleCityChange(e, 'shipping')}
                                    user={this.state.user}
                                    selectedCountry={this.state.user.shipping.country_code}
                                    countries={this.state.countries}
                                    cities={this.state.cities.shipping}
                                    strings={this.props.strings} fields={["address_1", "address_2", "area", "country", "zip"]} />
                                <CustomModal
                                    shippingOrBilling={'billing'}
                                    handleHide={() => this.handleHide('billing')}
                                    show={this.state.billing}
                                    handleSubmit={(e) => this.handleSubmit(e, 'billing')}
                                    handleChange={(e) => this.handleChange(e, 'billing')}
                                    selectedCountry={this.state.user.billing.country_code}
                                    handleCountryChange={(e) => this.handleCountryChange(e, 'billing')}
                                    handleCityChange={(e) => this.handleCityChange(e, 'billing')}
                                    countries={this.state.countries}
                                    cities={this.state.cities.billing}
                                    user={this.state.user}
                                    strings={this.props.strings} fields={["address_1", "address_2", "area", "country", "zip"]} />
                            </div>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}
export default AccountSetting;
import React from "react";
import { Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, NavItem, Navbar, Carousel } from 'react-bootstrap'
import Tooltip from "react-simple-tooltip"
import { tempProduct } from '../../services/TempObjects'
import { toPrice,toCloudinary } from '../../services/CommonServices'
import { Link } from 'react-router-dom'
import './ProductItem.css'
import {get} from 'lodash';
class ProductItem extends React.Component {
    render() {
        const tooltipOptions = {
            arrow: 15,
            background: "#2F2E33",
            border: "#000",
            color: "#fff",
            fadeDuration: 400,
            fadeEasing: "linear",
            fixed: false,
            fontFamily: "inherit",
            fontSize: "inherit",
            offset: 0,
            padding: 10,
            placement: "top",
            radius: 0,
            zIndex: 1,
            style: { fontSize: 16, lineHeight: 1, width: 40, height: 40 }
        }
        const product = this.props.product ? (this.props.product) : (tempProduct)
        return (
            <div className="pro-item-vertical">
                <div className="product-wrapper">
                    <div className="product-img">
                        <Link to={"product/" + product._id}>
                            {/* <div class="sale-tag"> */}
                            {/* <span class="new">new</span> */}
                            {/* <span class="sale">sale</span> */}
                            {/* </div> */}
                            <img src={product.images[0] ? (toCloudinary(product.images[0],null,230)) : require("../../img/Bab Makkah logo.png")}
                             onError={e => {
                                e.target.src = require("../../img/Bab Makkah logo.png");
                              }}
                            />
                        </Link>
                        {/* <div className="product-action">
                            <a>
                                <Tooltip
                                    content={"Quick view"}
                                    {...tooltipOptions}
                                >

                                    <i class="material-icons">
                                        search
                                    </i>
                                </Tooltip>
                            </a>
                            <a>
                                <Tooltip
                                    content={"Add to cart"}
                                    {...tooltipOptions}
                                >
                                    <i class="material-icons">
                                        add_shopping_cart
                                </i>
                                </Tooltip>
                            </a>
                            <a >
                                <Tooltip
                                    content={"Add to wishlist"}
                                    {...tooltipOptions}
                                >
                                    <i class="material-icons">
                                        favorite
                                </i>
                                </Tooltip>
                            </a>
                        </div> */}
                    </div>
                    <div className="product-content pt-15">
                        {this.props.checkout ?
                            null :
                            <h4>
                                <Link to={"/shop?category=" + product.type.category._id + "&type=" + product.type._id}>{product.type.name[sessionStorage.getItem("lang")]}</Link>
                            </h4>
                        }
                        <Link to={"product/" + product._id}>
                            <h2>
                                {product.name[sessionStorage.getItem("lang")]}
                            </h2>
                        </Link>
                        <div className="product-meta">
                            {/* <div className="pro-rating f-right">
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                </div> */}
                            <div className="pro-price">
                                <span>{toPrice(get(product.sizes[0], 'price', 0))} SAR</span> 
                                 {/* <span className="old-price">$79.00</span> */}
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        );
    }
}
export default ProductItem;
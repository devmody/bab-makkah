import React from "react";
import './Terms.css'
import { Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, ControlLabel, FormGroup, Navbar, Carousel, Tabs, Tab, FormControl } from 'react-bootstrap'
class Terms extends React.Component {
    constructor(props) {
        super(props)
    }
    componentDidMount() {
        window.scroll(0, 0)
    }

    render() {
        return (
            <div className={sessionStorage.getItem('lang') === "en" ? 'Terms ' : 'Terms-ar'}>
                <Grid>
                    <h3> {this.props.strings.TermsConditions} </h3>
                    <p> {this.props.strings.pterms} </p>
                    <h4> {this.props.strings.dev} </h4>
                    <p>
                        {this.props.strings.site}
                        <br />
                        {this.props.strings.User}
                        <br />
                        {this.props.strings.Seller}
                        <br />
                        {this.props.strings.Buyer}
                    </p>
                    <h4> {this.props.strings.EligibilityforUsage} </h4>
                    <p>  {this.props.strings.Anyperson} <br />
                        {this.props.strings.Usersare} <br />
                        {this.props.strings.Anyfinancial} <br />
                        {this.props.strings.Sellerswill} <br />
                    </p>
                    <h4>{this.props.strings.UsageofWebsite} </h4>
                    <h5> {this.props.strings.General} </h5>
                    <p> {this.props.strings.Allinformation} <br />
                        {this.props.strings.Nocontent} <br />
                        {this.props.strings.Noreligious} <br />
                        {this.props.strings.TheIntellectual} <br />

                    </p>
                    <h5> {this.props.strings.Sellers} </h5>
                    <p>  {this.props.strings.Sellershould} <br />
                        {this.props.strings.Allproducts} <br />
                        {this.props.strings.Descriptionof} <br />
                        {this.props.strings.Sellersshouldnotmislead} <br />
                        {this.props.strings.Sellersshouldremain} <br />
                        {this.props.strings.Packageeach} <br />
                    </p>

                    <h5>{this.props.strings.Fees}</h5>
                    <p> {this.props.strings.Registrationfor} <br />
                        {this.props.strings.BabMakkahdoesnotcharg} <br />
                        {this.props.strings.Listingofproducts} <br />

                    </p>
                    <h5> {this.props.strings.QualityWarranty} </h5>
                    <p> {this.props.strings.Warrantyofproducts} <br />
                        {this.props.strings.Complaintsonquality} <br />
                        {this.props.strings.AlthoughBabMakkahdoes} <br />
                        {this.props.strings.Incaseaproductdoes} <br />
                    </p>
                    <h5> {this.props.strings.FulfilmentofOrder} </h5>
                    <p>
                        {this.props.strings.TheSellerhastheoption} <br />
                        {this.props.strings.Customerswillbereimbursed}
                        <br />
                    </p>
                    <h5> {this.props.strings.GoverningLawJurisdiction} </h5>
                    <p>
                        {this.props.strings.Abreachofanyofthe} <br />
                        {this.props.strings.Anydisputearising} <br />

                        {this.props.strings.AccordinglytheUser} <br />

                    </p>


                </Grid>
            </div>
        );
    }
}
export default Terms;


import React from "react";
import { getAllObjects, toCloudinary } from "../../services/CommonServices";
import { Link } from 'react-router-dom'


class HomeBanner extends React.Component {

  constructor() {
    super()
    this.state = {
      featuredContent: [{
        name: {
          en: "Dates",
          ar: "عجوات"
        },
        title: {
          en: "Packed Dates",
          ar: "تمور معلبة"
        },
        description: {
          en: "100% Organic",
          ar: "١٠٠٪ طبيعية"
        },
      }, {
        name: {
          en: "Handmade",
          ar: "صناعة يدوية"
        },
        title: {
          en: "Islamic Jewelry",
          ar: "مجوهرات اسلامية"
        },
        description: {
          en: "Highest Quality",
          ar: "أعلى جودة"
        },
      }, {
        name: {
          en: "Woodworks",
          ar: "خشبيات"
        },
        title: {
          en: "Stunning Wood art",
          ar: "نقوش وزخارف رائعة"
        },
        description: {
          en: "Local Artists",
          ar: "فنانين محليين"
        },
      },]
    }
  }
  componentDidMount() {
    getAllObjects('category/featured')
      .then((response) => {
        this.setState({ featuredContent: response.data })
      })
  }
  render() {
    return (
      <section className="banner-area-full pt-80">
        <div className="row">
          {
            this.state.featuredContent.map((category) => (
              <div className="col-md-4">
                <Link to={"/shop?category=" + category._id}>
                  <div className="featured-wrap block1">
                    <div className="featured-img">
                      <img src={toCloudinary(category.featured_image, 200, 360)|| require("../../img/Bab Makkah logo.png")}
                        onError={e => {
                          e.target.src = require("../../img/Bab Makkah logo.png");
                        }}
                      />

                    </div>
                    <div className="featured-content">
                      {/* <h3>{this.state.featuredContent[0].name[sessionStorage.getItem("lang")]}</h3> */}
                      {/* <h2>{category.name[sessionStorage.getItem("lang")]}</h2> */}
                      {/* <h5>{this.state.featuredContent[0]['description'][sessionStorage.getItem("lang")]}</h5> */}
                    </div>
                  </div>
                </Link>
              </div>
            ))
          }
        </div>
      </section>
    );
  }
}

export default HomeBanner;




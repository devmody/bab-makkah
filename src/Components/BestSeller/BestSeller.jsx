import React from "react";
import { Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, NavItem, Navbar, Carousel, Tabs, Tab } from 'react-bootstrap'
import BestProductItem from "../BestProductItem/BestProductItem";
import './BestSeller.css'
import { getAllObjects } from "../../services/CommonServices";
class BestSeller extends React.Component {
  state={
    newProducts:[],
    randomProducts:[],
    featuredProducts:[],
  }
  componentDidMount(){
    getAllObjects('product/new')
    .then((response)=>{
      this.setState({newProducts:response.data})
    })
    getAllObjects('product/random')
    .then((response)=>{
      this.setState({randomProducts:response.data})
    })
    getAllObjects('product/random-featured')
    .then((response)=>{
      this.setState({featuredProducts:response.data})
    })
  }
  render() {
    return (
      <div className="BestSeller">
        <div className="container">
            <Col style={{background:'white'}} md={4}>
              <div className="section-title__holder">
                <div className="section-title">
                  <h3>{this.props.strings.bestSellers}</h3>
                </div>
              </div>
              {
                this.state.randomProducts.map((product)=>(
                  <BestProductItem product={product}/>
                ))
              }
            </Col>
            <Col style={{background:'white'}} md={4}>
              <div className="section-title__holder">

                <div className="section-title">
                  <h3>{this.props.strings.newArrivals}</h3>
                </div>
              </div>
              {
                this.state.newProducts.map((product)=>(
                  <BestProductItem product={product}/>
                ))
              }
            </Col>
            <Col style={{background:'white'}} md={4}>
              <div className="section-title__holder">
                <div className="section-title">
                  <h3>{this.props.strings.topViewed}</h3>
                </div>
              </div>
              {
                this.state.featuredProducts.map((product)=>(
                  <BestProductItem product={product}/>
                ))
              }
            </Col>
        </div>
      </div>
    );
  }
}
export default BestSeller;
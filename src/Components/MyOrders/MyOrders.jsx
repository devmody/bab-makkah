import React from "react";
import { Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, NavItem, Navbar, Carousel, Modal, FormGroup, FormControl, ControlLabel } from 'react-bootstrap'
import ProductItem from "../ProductItem/ProductItem";
import AccountSidebar from "../AccountSidebar/AccountSidebar";
import AccountHeader from "../AccountHeader/AccountHeader";
import Breadcrumbs from "../Breadcrumbs/Breadcrumbs";
import { toast, Flip } from 'react-toastify';
import { Subscribe } from "unstated";
import Spinner from 'react-spinkit'
import CartContainer from "../../Cart";
import { getSingleObject, getObjectWithToken, toPrice, toCloudinary, deleteObject } from "../../services/CommonServices";
class MyOrders extends React.Component {
    toastSettings = {
        position: "top-right",
        autoClose: 5000,
        transition: Flip,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        ready: false,
        className: "sub-toast"
    }
    constructor(props, context) {
        super(props, context);
        this.calulateTotalAmount = this.calulateTotalAmount.bind(this)
        this.calulateSubTotalAmount = this.calulateSubTotalAmount.bind(this)
        this.determineStatus = this.determineStatus.bind(this)
        this.state = {
            orders: [],

        };
    }
    determineStatus(status) {
        if (Number(status) === 0) {
            return "Processing"
        }
        if (Number(status) === 1) {
            return "Processing"
        }
        if (Number(status) === 2) {
            return "Packed"
        }
        if (Number(status) === 3) {
            return "Out For Delivery";
        }
        if (Number(status) === 4) {
            return "Delivered"
        }
        if (Number(status) === 5) {
            return "Delivered"
        }
        if (Number(status) === -1) {

            return "Cancelled"
        }
        return "Refunded"
    }
    calculateDiscount(){

    }
    calulateTotalAmount(total, shippingCost) {
        total += shippingCost 
        return total
    }
    calulateSubTotalAmount(total, shippingCost) {
        // total += shippingCost
        let subTotal = total - shippingCost - 18
        return subTotal
    }
    componentDidMount() {
        window.scroll(0, 0)
        getObjectWithToken('order/users')
            .then(response => {
                console.log(response.data)
                this.setState({ orders: response.data, ready: true })
                console.log('orderss',this.state.orders);
            })
            .catch(err => {
                console.log(err)
            })

        this.cart.refreshCart()

    }

    componentDidUpdate() {
        window.scroll(0, 0)

    }

    render() {
        console.log('props', this.props);
        return (
            <Subscribe to={[CartContainer]}>{cart => {
                this.cart = cart
                return (
                    <div className="account-wishlist">
                        <Grid>
                            <Row>
                                <Col md={3}>
                                    <AccountSidebar strings={this.props.strings} />
                                </Col>
                                <Col md={9}>
                                    <div className="cart-border">
                                        {console.log('orders', this.state.orders)}
                                        {
                                            this.state.ready ?
                                                this.state.orders.length == 0 ?
                                                    <h3>{this.props.strings.emptyOrders}</h3>
                                                    :
                                                    this.state.orders.map((order, key) => (
                                                        
                                                        <Row>
                                                            <Col md={4}>
                                                                <div className="cart-product__image">
                                                                    <img src={order.products[0].product ? order.products[0].product.images[0] ? (toCloudinary(order.products[0].product.images[0], null, 248)) : require("../../img/Bab Makkah logo.png") : require("../../img/Bab Makkah logo.png")}
                                                                        onError={e => {
                                                                            e.target.src = require("../../img/Bab Makkah logo.png");
                                                                        }}
                                                                    />
                                                                </div>
                                                            </Col>
                                                            <Col md={6}>
                                                                <div className="cart-details">
                                                                    <div className="cart-item">
                                                                        <h4> {this.props.strings.orderId + ': '}<span>{order.orderId}</span></h4>
                                                                    </div>
                                                                    {
                                                                        <div className="cart-item">
                                                                            <h4> {this.props.strings.vendorName + ': '} {order.vendor ? (<span>{!order.vendor.name || order.vendor.name[sessionStorage.getItem("lang")]}</span>) : (this.props.strings.noVendor)}</h4>
                                                                        </div>
                                                                    }

                                                                    <div className="cart-item">
                                                                        <h4> {this.props.strings.orderDate}: <span>{new Date(order.created_at).toLocaleDateString(sessionStorage.getItem('lang') === 'en' ? 'en-US' : 'ar-EG')}</span></h4>
                                                                    </div>
                                                                    <div className="cart-item">
                                                                        <h4>{this.props.strings.status}: <span>{this.determineStatus(order.status)}</span></h4>
                                                                    </div>
                                                                    {
                                                                        order.shippingCost ?
                                                                            <React.Fragment>
                                                                                <div className="pro-price f-left">
                                                                                    <span>{this.props.strings.sub}: {toPrice(this.calulateSubTotalAmount(order.total, order.shippingCost))} SAR</span>
                                                                                </div>
                                                                                <div className="pro-price f-left">
                                                                                    <span>{this.props.strings.shipping}: {toPrice(order.shippingCost)} SAR</span>
                                                                                </div>
                                                                                {order.paymentReference === "" ? 
                                                                                <div className="pro-price f-left">
                                                                                <span>{this.props.strings.cashOnDelivery}: {toPrice(18)} SAR</span>
                                                                            </div>
                                                                            :
                                                                            ""}
                                                                                

                                                                            </React.Fragment>
                                                                            :
                                                                            <div />
                                                                    }
                                                                    {/* <div className="pro-price f-left">
                                                            <span>{this.props.strings.total}: {toPrice(order.total)} SAR</span>
                                                        </div> */}
                                                                    <div className="pro-price f-left">
                                                                        {/* <span>{this.props.strings.total}: {order.shippingCost ? toPrice(this.calulateTotalAmount(order.total, order.shippingCost)) : toPrice(order.total)} SAR</span> */}
                                                                        <span>{this.props.strings.total}: {toPrice(order.total)} SAR</span>
                                                                    </div>

                                                                    {/*<Button style={{display:'none'}}className="main-btn account-wishlit_btn"> Track Shipment </Button>*/}
                                                                </div>
                                                            </Col>
                                                            <div className="my_orders_btn">
                                                                <Button onClick={() => this.props.history.push({ pathname: "viewProducts/" + order._id })} className="main-btn MyOrder_viewProducts"> {this.props.strings.viewProducts} </Button>
                                                            </div>
                                                        </Row>

                                                    )) :
                                                (<Spinner style={{ height: 1587 }} fadeIn={0} color="#2F2E33" name="rotating-plane" />)
                                        }
                                    </div>
                                </Col>
                            </Row>
                        </Grid>
                    </div>

                )
            }
            }
            </Subscribe >
        );
    }
}
export default MyOrders;
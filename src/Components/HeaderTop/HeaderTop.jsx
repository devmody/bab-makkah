import React from "react";
import { Row, Col, Grid, Button, DropdownButton, MenuItem,ToggleButton,ButtonToolbar ,ToggleButtonGroup} from 'react-bootstrap'

class HeaderTop extends React.Component {
  constructor() {
    super();
  }
  componentDidMount(){
    if(sessionStorage.getItem("lang")==="ar"){
      document.getElementById("arabicBtn").click()          
    } else {
      document.getElementById("englishBtn").click()    
    }
  }
  render() {
    return (
      <div>
        <div className="header-top-area">
          <div className="container">
            <div className="row">
              <div className=" col-md-9 col-xs-6">
              <div className="header-top-area__social-links">
                <div className="header-top-area__social-icon"><a href="https://facebook.com/babmakkahstores" target="_blank" ><i className="fa fa-facebook"></i></a></div>
                <div className="header-top-area__social-icon"><a href="https://instagram.com/babmakkahstores" target="_blank" ><i className="fa fa-instagram"></i></a></div>
                <div className="header-top-area__social-icon"><a href="https://twitter.com/babmakkahstores" target="_blank" ><i className="fa fa-twitter"></i></a></div>
              </div>
                {/*<div className="header-info">
                  <span>
                    <i className="fa fa-phone" /> <a href="tel:+966 533 571 555" class="phoneNumber">&#x200E;+966 533 571 555</a>
                  </span>
                  <span>
                    <i className="fa fa-envelope" /> <a href="mailto:info@babmakkah.com">info@babmakkah.com</a>
                  </span>
    </div>*/}
              </div>
              <div className="col-md-3  col-xs-6">
              {/* <div className="header-language">
              <DropdownButton title="English" id="dropdown-size-medium">
      <MenuItem eventKey="1">English</MenuItem>
      <MenuItem eventKey="2">عربي</MenuItem>
    </DropdownButton>
    </div> */}

       <ButtonToolbar className="Language-choose">
                  <ToggleButtonGroup onChange={(value)=>this.props.onLangChange(value)} type="radio" name="options" defaultValue={'en'}>
                    <ToggleButton id="englishBtn" value={'en'}>English</ToggleButton>
                    
                    <ToggleButton id="arabicBtn" value={'ar'}>عربى</ToggleButton>
                  </ToggleButtonGroup>
                </ButtonToolbar>


              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default HeaderTop;

import React from "react";
import {
  Row,
  Col,
  Grid,
  Button,
  DropdownButton,
  MenuItem
} from "react-bootstrap";
import { Link, withRouter } from "react-router-dom";
import queryString from "query-string";
import CartContainer from "../../Cart";
import { Subscribe } from "unstated";
import { toPrice, getObjectWithToken } from '../../services/CommonServices'
class HeaderBottom extends React.Component {
  state = {
    selectedCategory: { name: { en: "All categories", ar: "جميع الأقسام" } },
    title: "All categories",
    //searchTerm: ''
  };

  checkLang = (e) => {
    if(sessionStorage.getItem("lang")==="ar"){
    const ARreg = /[\u0600-\u06FF]/
    if (!ARreg.test(e.key)) {
      e.preventDefault();
    }
  }
  else if (sessionStorage.getItem("lang")==="en")
  {
    const ENreg = /[a-zA-Z]+/g
    if (!ENreg.test(e.key)) {
      e.preventDefault();
    }
  }
  } 
  componentDidMount(){
   // this.props.refreshFunction()
  }
  render() {
    return (
      <Subscribe to={[CartContainer]}>
        {
          cart => (
            <div>
              <div className="header-bottom">
                <div className="container">
                  <div className="row align-item">
                    <Col md={3}>
                      <div className="header-info">
                        <Link to="/">
                          <img src={require("../../img/logo.png")} />
                        </Link>
                      </div>
                    </Col>
                    <Col md={6}>
                      <div className="header__search">
                        <div className="search-cat clearfix">
                          <div className="cat-select">
                            <div className="nice-select">
                              <DropdownButton
                                bsSize="xsmall"
                                onSelect={key =>
                                  this.setState({
                                    selectedCategory: this.props.categories[key]
                                  })
                                }
                                title={
                                  this.state.selectedCategory.name[
                                  sessionStorage.getItem("lang")
                                  ]
                                }
                                id="dropdown-size-extra-small"
                              >
                                {this.props.categories.map((category, key) => (
                                  <MenuItem eventKey={key}>
                                    {category.name[sessionStorage.getItem("lang")]}
                                  </MenuItem>
                                ))}
                              </DropdownButton>
                            </div>
                          </div>
                          <div className="cat-form">
                            <form
                              onSubmit={e => {
                                e.preventDefault();
                                this.props.history.push({
                                  pathname: "/shop",
                                  search: queryString.stringify(
                                    Object.assign(
                                      queryString.parse(this.props.location.search),
                                      {
                                        category: this.state.selectedCategory._id,
                                        search: this.state.searchTerm
                                          ? this.state.searchTerm
                                          : undefined
                                      }
                                    )
                                  )
                                });
                                this.setState({searchTerm : ''});
                              }}
                            >
                              <input
                                value={this.state.searchTerm}

                                // onKeyPress={(e) => this.checkLang(e)}
                                onChange={e =>
                                  this.setState({ searchTerm: e.target.value })
                                }
                                type="text"
                                placeholder={this.props.strings.search + "..."}
                              />
                              <Button type="submit" bsStyle="primary">
                                {this.props.strings.search}
                              </Button>
                            </form>
                          </div>
                        </div>
                      </div>
                    </Col>
                    <Col md={3}>
                      <Row>
                        <Col mdOffset={cart.state.loggedIn ? (0) : 4} md={4}>
                          <div className="header-bottom__controller right">
                            <div>
                              <Link to="/cart">
                                <img src={require("../../img/bag-home.png")} />
                              </Link>
                            </div>
                            <div>
                              <Link to="/cart"
                                onClick={() => {
                                  this.props.history.push({ pathname: "/cart" });
                                }}
                              >
                                <h5> {this.props.strings.cart} </h5></Link>
                              <Subscribe to={[CartContainer]}>
                                {
                                  cart => (
                                    <p className="header-bottom__item">
                                      {cart.state.count} {this.props.strings.items}
                                      <div>
                                        {/* <em>{toPrice(cart.state.total)} SAR</em> */}
                                      </div>
                                    </p>
                                  )
                                }
                              </Subscribe>
                            </div>
                          </div>
                        </Col>
                        {
                          cart.state.loggedIn ?
                          
                            <Col md={4}>
                              <div className="header-bottom__controller flex-col">
                                <div>
                                  <Link exact to="/account">
                                    <img src={require("../../img/home-user.png")} />
                                  </Link>
                                </div>
                                <div style={{ cursor: "pointer" }}>


                                  <Link to="/account"
                                    onClick={() => {
                                      this.props.history.push({ pathname: "/account" });
                                    }}
                                  >
                                    {
                                      cart.state.name ?
                                        <h5>{this.props.strings.welcomeUser + ", " + cart.state.name.split(' ').slice(0, 1)}</h5>
                                        :
                                        <h5>{this.props.strings.welcomeUser}</h5>
                                    }
                                  </Link>

                                </div>
                              </div>
                            </Col>

                            : ('')
                        }
                        <Col md={4}>
                          <div className="header-bottom__controller flex-col">
                            <div>
                              <Link exact to="/login">
                                <img src={require("../../img/home-key.png")} />
                              </Link>
                            </div>
                            {cart.state.loggedIn ? (
                              <div style={{ cursor: "pointer" }}>
                                <a
                                  onClick={() => {
                                    localStorage.removeItem("token");
                                    this.props.history.push({ pathname: "/login" });
                                  }}
                                >
                                  <h5>{this.props.strings.logout}</h5>
                                </a>
                              </div>
                            ) : (
                                <div>
                                  <a
                                    onClick={() => {
                                      localStorage.removeItem("token");
                                      this.props.history.push({ pathname: "/login" });
                                    }}
                                  >
                                    <h5>{this.props.strings.register}</h5>
                                  </a>
                                  <a
                                    onClick={() => {
                                      localStorage.removeItem("token");
                                      this.props.history.push({ pathname: "/login" });
                                    }}
                                  >
                                    <p className="header-bottom__item">
                                      {this.props.strings.or} {this.props.strings.login}{" "}
                                    </p>
                                  </a>
                                </div>
                              )}
                          </div>
                        </Col>
                      </Row>
                    </Col>
                  </div>
                </div>
              </div>
            </div>
          )
        }
      </Subscribe>
    );
  }
}

export default withRouter(HeaderBottom);

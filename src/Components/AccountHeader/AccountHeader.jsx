import React from "react";
import { Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, NavItem, Navbar, Carousel } from 'react-bootstrap'
import ProductItem from "../ProductItem/ProductItem";
import AccountSidebar from "../AccountSidebar/AccountSidebar";
import DropzoneS3Uploader from 'react-dropzone-s3-uploader'
import { getSingleObject, updateObjectWithToken,toCloudinary} from "../../services/CommonServices";
import { toast, Flip } from 'react-toastify';
import config from '../../../src/config';

class AccountHeader extends React.Component {

	toastSettings = {
		position: "top-right",
		autoClose: 5000,
		transition: Flip,
		hideProgressBar: true,
		closeOnClick: true,
		pauseOnHover: true,
		draggable: false,
		className: "sub-toast"
	}

	constructor(props) {
		super(props)
		this.state = {
			progress: 0,
		};

	}

	handleFinishedUpload = info => {
		console.log('File uploaded with filename', info.filename)
		console.log('Access it on s3 at', info.fileUrl)
		this.setState({ progress: 0 })
		updateObjectWithToken('users/profile', { image: info.fileUrl })
			.then(response => {
				this.props.componentDidMount()
				toast.success('Image uploaded', this.toastSettings)
			})
			.catch(err => {
				toast.error('An error has occured', this.toastSettings)
			})
	}

	render() {
		const uploadOptions = {
			server: config.API_URL.slice(0,-1),
			s3path: 'users/' + this.props.user._id + '/',
			signingUrlWithCredential: true,
			autoUpload: true,
			signingUrlQueryParams: { uploadType: 'avatar' }
		}
		const s3Url = 'https://bab-makkah-static.s3.amazonaws.com/'
		return (
			<div className="Account-header">
				<Grid>
					<Row>
						<Col md={5}>
							<div className="Account-header__holder">
								<div className="Account-avatar">
									<DropzoneS3Uploader
										className="dropzone_profile_image"
										style={{ width: 'inherit', height: 'inherit', border: 'none', cursor: 'pointer' }}
										onFinish={this.handleFinishedUpload}
										onProgress={(progress) => { this.setState({ progress: progress }) }}
										s3Url={s3Url}
										maxSize={1024 * 1024 * 5}
										upload={uploadOptions}
									>
										<img src={this.props.user.image ? toCloudinary(this.props.user.image,160,160) : require("../../img/Bab Makkah logo.png")}
										 onError={e => {
											e.target.src = require("../../img/Bab Makkah logo.png");
										}}
										/>
									</DropzoneS3Uploader>
								</div>
								<div className="Account-data">
									<h3>{this.props.user.name ? this.props.user.name : ''} </h3>
									<p> {this.props.user.email ? this.props.user.email : ''} </p>

								</div>
							</div>
						</Col>
					</Row>
				</Grid>


			</div>
		);
	}
}

export default AccountHeader;



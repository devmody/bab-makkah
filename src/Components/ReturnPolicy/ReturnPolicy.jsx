import React from "react";
import './ReturnPolicy.css'
import { Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, ControlLabel, FormGroup, Navbar, Carousel, Tabs, Tab, FormControl } from 'react-bootstrap'
class ReturnPolicy extends React.Component {
    constructor(props) {
        super(props)
    }
    componentDidMount() {
        window.scroll(0, 0)
      }

    render() {
        return (
            <div className={sessionStorage.getItem('lang')==='en'?'return-policy':'return-policy-ar'}>
                <Grid>
                    
                <h3>{this.props.strings.ReturnPolicy}</h3>
                <p>{this.props.strings.ReturnPolicyDesc1}</p>
                <p>{this.props.strings.ReturnPolicyDesc2}</p>
                <p>{this.props.strings.ReturnPolicyDesc3}</p>
                <p>{this.props.strings.ReturnPolicyDesc4}</p>
                </Grid>
            </div>
        );
    }
}
export default ReturnPolicy;
import React from "react";
import './CancellationPolicy.css'
import { Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, ControlLabel, FormGroup, Navbar, Carousel, Tabs, Tab, FormControl } from 'react-bootstrap'
class CancellationPolicy extends React.Component {
    constructor(props) {
        super(props)
    }
    componentDidMount() {
        window.scroll(0, 0)
    }

    render() {
        return (
            <div className={sessionStorage.getItem('lang') === "en" ? 'cancellation-policy' : 'cancellation-policy-ar'}>
                <Grid>
                    <h3>
                        {this.props.strings.CancellationPolicy1}
                    </h3>
                    <p>  {this.props.strings.Anyordercan} <br />
                        {this.props.strings.Afterprocessof} <br />
                        {this.props.strings.Insomecase} <br />
                    </p>
                </Grid>
            </div>
        );
    }
}
export default CancellationPolicy;
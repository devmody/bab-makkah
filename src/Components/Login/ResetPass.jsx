import React from "react";
import { Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, NavItem, Navbar, Carousel, Modal, FormGroup, FormControl, ControlLabel, Checkbox } from 'react-bootstrap'
import ProductItem from "../ProductItem/ProductItem";
import AccountSidebar from "../AccountSidebar/AccountSidebar";
import AccountHeader from "../AccountHeader/AccountHeader";
import { postObject } from "../../services/CommonServices";
import './Login.css'
import axios from 'axios'
import config from '../../config'
import { withRouter } from 'react-router-dom'
import { toast, Flip } from 'react-toastify';

class Login extends React.Component {
    toastSettings = {
        position: "top-right",
        autoClose: 5000,
        transition: Flip,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        className: "sub-toast"
    }
    state = {
        phone_number_login: '',
        password_login: '',
        phone_number_signup: '',
        password_signup1: '',
        password_signup2: '',
    }
    handleLogin(e) {
        e.preventDefault();
        toast.dismiss()
        if (!this.state.phone) {
            toast.error(this.props.strings.phoneValidation, this.toastSettings)
            return;
        }
        if (!this.state.password_1) {
            toast.error(this.props.strings.passwordValidation, this.toastSettings)
            return;
        }
        if (this.state.password_1 !== this.state.password_2) {
            toast.error(this.props.strings.passwordMatchValidation, this.toastSettings)
            return;
        }
        window.resetPassword(this.state.phone, this.state.password_1, axios, config.API_URL, this.props.history)
    }
    render() {
        return (
            <div className="Login Reset">
                <div className="login-area gray-bg">
                    <div className="container">
                        <Row>
                            <Col mdOffset={3} md={6}>
                                <div className="login-content mb-30">
                                    <h2 className="login-title">{this.props.strings.resetPassword}</h2>
                                    <form onSubmit={this.handleLogin.bind(this)}>
                                        <label>{this.props.strings.phoneNumber}</label>
                                        <FormControl type="text" onChange={(e) => this.setState({ phone: e.target.value })} placeholder="0123456789" />
                                        <label>{this.props.strings.password}</label>
                                        <FormControl type="password" onChange={(e) => this.setState({ password_1: e.target.value })} placeholder="******" />
                                        <label>{this.props.strings.confirmPassword}</label>                                        
                                        <FormControl type="password" onChange={(e) => this.setState({ password_2: e.target.value })} placeholder="******" />
                                        <input className="login-sub main-btn" type="submit" value={this.props.strings.login} />
                                    </form>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </div>
            </div>
        );
    }
}
export default withRouter(Login);
import React from "react";
import { Row, Col, FormControl } from 'react-bootstrap'
import { postObject, transferCart, getObjectWithToken } from "../../services/CommonServices";
import './Login.css'
import axios from 'axios'
import config from '../../config'
import { withRouter, Link } from 'react-router-dom'
import { toast, Flip } from 'react-toastify';
import { Subscribe } from "unstated";
import CartContainer from "../../Cart";

class Login extends React.Component {
    toastSettings = {
        position: "top-right",
        autoClose: 5000,
        transition: Flip,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        className: "sub-toast"
    }
    state = {
        phone_number_login: '',
        password_login: '',
        phone_number_signup: '',
        password_signup1: '',
        password_signup2: '',
        policy_checked: false,
        //loggedin: false
    }
    handleSignUp(e, cart) {
        e.preventDefault();
        toast.dismiss()
        if (!this.state.phone_number_signup) {
            toast.error(this.props.strings.phoneValidation, this.toastSettings)
            return;
        }
        if (isNaN(this.state.phone_number_signup)) {
            toast.error(this.props.strings.inValidPhone, this.toastSettings)
            return;
        }
        if (!this.state.password_signup1) {
            toast.error(this.props.strings.passwordValidation, this.toastSettings)
            return;
        }
        if (!this.state.password_signup2) {
            toast.error(this.props.strings.passwordConfirmValidation, this.toastSettings)
            return;
        }
        if (!(this.state.password_signup1 === this.state.password_signup2)) {
            toast.error(this.props.strings.passwordMatchValidation, this.toastSettings)
            return
        }
        if (!this.state.policy_checked) {
            toast.error(this.props.strings.acceptPolicy, this.toastSettings)
            return;
        }
        
        //cart.state.loggedIn = true;
        window.smsLogin(this.state.phone_number_signup.trim(), this.state.password_signup1, axios, config.API_URL, this.props.history, (message) => {
            console.log('before toast')
            postObject("users/login", {
                phone_number: this.state.phone_number_login.trim(),
                password: this.state.password_login.trim()
    
            })
                .then((response) => {
                    console.log('res data', response.data)
                    if (response.data.token) {
                        localStorage.setItem("token", response.data.token)
                        getObjectWithToken('users/cart').then((response) => {
                            transferCart(response.data.cart.products, cart.refreshCart)
                        }).catch((err) => console.log(err))
                        cart.refreshCart()
                        toast.success(this.props.strings.loginSuccess, this.toastSettings)
                        this.props.history.push({
                            pathname: '/',
                        })
                    } else {
                        toast.error(this.props.strings.wrongCredentials, this.toastSettings)
                    }
                })
            console.log('after transfer');
            toast.error(message, config.toastSettings)
        })

       // cart.state.loggedIn = true;
        //transferCart(cart.products, cart.refreshCart)

    }

    handleLogin(e, cart) {
        e.preventDefault();
        toast.dismiss()
        if (!this.state.phone_number_login) {
            toast.error(this.props.strings.phoneValidation, this.toastSettings)
            return;
        }
        if (isNaN(this.state.phone_number_login)) {
            toast.error(this.props.strings.inValidPhone, this.toastSettings)
            return;
        }
        if (!this.state.password_login) {
            toast.error(this.props.strings.passwordValidation, this.toastSettings)
            return;
        }
        postObject("users/login", {
            phone_number: this.state.phone_number_login.trim(),
            password: this.state.password_login.trim()

        })
            .then((response) => {
                console.log('res data', response.data)
                if (response.data.token) {
                    localStorage.setItem("token", response.data.token)
                    getObjectWithToken('users/cart').then((response) => {
                        transferCart(response.data.cart.products, cart.refreshCart)
                    }).catch((err) => console.log(err))
                    cart.refreshCart()
                    toast.success(this.props.strings.loginSuccess, this.toastSettings)
                    this.props.history.push({
                        pathname: '/',
                    })
                } else {
                    toast.error(this.props.strings.wrongCredentials, this.toastSettings)
                }
            })
    }
    componentDidMount() {
        window.scroll(0, 0)
        localStorage.removeItem('token')
        console.log('cart', this.cart)
        if (this.cart) {
            this.cart.refreshCart()
        } else {
            console.log("no cart")
            // this.cart.refreshCart()
        }
    }

    render() {
        return (
            <Subscribe to={[CartContainer]}>{cart => {
                this.cart = cart
                return (
                    <div className="Login">
                        <div className="login-area gray-bg">
                            <div className="container">
                                <Row>
                                    <Col md={6}>
                                        <div className="login-content mb-30">
                                            {/* <h2 className="login-title">{this.props.strings.login} <span className="are-vendor"><a href="https://dashboard.bab-makkah.qpix.io/">{this.props.strings.loginVen}</a></span></h2> */}
                                            <p>{this.props.strings.welcomeBack}</p>
                                            {/* <div className="social-sign">
     <a href="#">
     <i className="fa fa-facebook" /> Sign in with facebook</a>
     <a className="twitter" href="#">
     <i className="fa fa-twitter" /> Sign in with twitter</a>
 </div> */}
                                            <form onSubmit={(e) => this.handleLogin(e, cart)}>
                                                <label>{this.props.strings.phoneNumber}*</label>
                                                <FormControl type="text" onChange={(e) => this.setState({ phone_number_login: e.target.value })} placeholder="0523456789" />
                                                <label>{this.props.strings.password}*</label>
                                                <FormControl type="password" onChange={(e) => this.setState({ password_login: e.target.value })} placeholder="******" />
                                                <div className="login-lost">
                                                    {/* <span className="log-rem">
                             <FormGroup>
                                 <Checkbox>Remember me</Checkbox>
                             </FormGroup>
                         </span> */}
                                                    <span className="forgot-login">
                                                        <Link to="/reset-password">{this.props.strings.forgotYourpassword}</Link>
                                                    </span>
                                                </div>
                                                <input className="login-sub main-btn" type="submit" value={this.props.strings.login} />
                                            </form>
                                        </div>
                                    </Col>
                                    <div className="col-md-6">
                                        <div className="login-content mb-30">
                                            {/* <h2 className="login-title">{this.props.strings.register} <span className="are-vendor"><Link to="/vendor-register">{this.props.strings.areVendor}</Link></span></h2> */}
                                            <p>{this.props.strings.welcome}</p>
                                            <form onSubmit={(e) => this.handleSignUp(e, cart)}>
                                                <label>{this.props.strings.phoneNumber}*</label>
                                                <FormControl onChange={(e) => this.setState({ phone_number_signup: e.target.value })} type="text" placeholder="0523456789" />
                                                <label>{this.props.strings.password}*</label>
                                                <FormControl onChange={(e) => this.setState({ password_signup1: e.target.value })} type="password" placeholder="******" />
                                                <label>{this.props.strings.confirmPassword}*</label>
                                                <FormControl onChange={(e) => this.setState({ password_signup2: e.target.value })} type="password" placeholder="******" />
                                                <label>
                                                    <input id="policy" type="checkbox" checked={this.state.policy_checked} onChange={(e) => this.setState({ policy_checked: !this.state.policy_checked })} /> &nbsp;
                                                    {this.props.strings.iaccept} <Link to="/privacy-policy" target="_blank">{this.props.strings.terms}</Link>
                                                </label>
                                                <input className="login-sub" type="submit" value={this.props.strings.register} />
                                            </form>
                                        </div>
                                    </div>
                                </Row>
                            </div>
                        </div>
                    </div>
                )
            }}
            </Subscribe>
        );
    }
}
export default withRouter(Login);
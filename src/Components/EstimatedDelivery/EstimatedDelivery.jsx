import React from "react";
import './EstimatedDelivery.css'
import { Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, ControlLabel, FormGroup, Navbar, Carousel, Tabs, Tab, FormControl } from 'react-bootstrap'
class EstimatedDelivery extends React.Component {
    constructor() {
        super()
    }
    componentDidMount() {
        window.scroll(0, 0)
    }

    render() {
        return (
            <div className={sessionStorage.getItem("lang") === 'en' ? 'estimated-delivery' : 'estimated-delivery-ar'}>
                <Grid>

                    <h3>{this.props.strings.Estimateddeliverytime}</h3>

                    <p>{this.props.strings.Estimateddeliverytime1}</p>
                    <p>{this.props.strings.Estimateddeliverytime2}</p>
                    <p>{this.props.strings.Estimateddeliverytim3}</p>

                </Grid>
            </div>

        );
    }
}
export default EstimatedDelivery;
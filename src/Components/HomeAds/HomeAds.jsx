import React from "react";

import { Row, Col } from 'react-bootstrap'

class HomeAds extends React.Component {
    render() {
        return (
            <div className="HomeAds container">
                <div className="row">
                    <div className="col-xs-12">
                    {
                        this.props.bannerImage?(
                            <img src={this.props.bannerImage} />
                        ):('')
                    }
                    </div>
                </div>
                
            </div>
        );
    }
}

export default HomeAds;

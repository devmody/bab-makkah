import React from "react";
import { toast, Flip } from 'react-toastify';
import { postObject, getObjectWithToken } from "../../services/CommonServices";
import { Row, Col, ControlLabel, MenuItem, DropdownButton, FormGroup, FormControl, Grid, Button } from 'react-bootstrap'

const optionsEn = ["Order Cancellation", "Order Delay", "Inquiry", "Compliant", "Technical Issue", "Other", "Vendor Support"];
const optionsAr = ["الغاء الطلب", "تاخر الطلب", "استفسار", "شكوى", "عطل فنى", "اخرى", "دعم الموزع"];

class ContactForm extends React.Component {
    toastSettings = {
        position: "top-right",
        autoClose: 5000,
        transition: Flip,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        className: "sub-toast"
    }
    constructor(props) {
        super(props);
        this.changeValue = this.changeValue.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.state = {
            name: "",
            email: "",
            phone_number: "",
            message: "",
            subject: "",
            title: "Choose a Subject",
         
        };
    }

    changeValue(event, key) {
        this.setState({ [key]: event.target.value })
    }
    handleSubmit(e) {
        e.preventDefault();
        if (!this.state.name) {
            toast.error(this.props.strings.nameReq, this.toastSettings)
            return;
        }

        if (!this.state.email) {
            toast.error(this.props.strings.emailReq, this.toastSettings)
            return;
        }

        if (!this.state.email.includes("@")) {
            toast.error(this.props.strings.invalidMail, this.toastSettings)
            return;
        }
        if (!this.state.phone_number) {
            toast.error(this.props.strings.phoneValidation, this.toastSettings)
            return;
        }
        if (isNaN(this.state.phone_number)) {
            toast.error(this.props.strings.inValidPhone, this.toastSettings)
            return;
        }
        if (this.state.message==='') {
            toast.error("Message field is required", this.toastSettings)
            return;
        }

        if(this.state.title ===  'Choose a Subject'){
            toast.error(this.props.strings.chooseASubject, this.toastSettings)
            return;
        }

        postObject("contact", {
            email: this.state.email,
            name: this.state.name,
            message: this.state.message,
            phone_number: this.state.phone_number,
            subject: this.state.title,
            language: sessionStorage.getItem("lang")
        })
            .then((response) => {
                toast.success(this.props.strings.formSuccess, this.toastSettings)
            })
            .catch((err) => {
                toast.error(this.props.strings.formFailed, this.toastSettings)
            })
    }
    handleSelect(eventKey, event) {
        this.setState({ title: sessionStorage.getItem("lang") === 'en' ? optionsEn[eventKey]  : optionsAr[eventKey]});
    }
    componentDidMount() {
        window.scroll(0, 0)
        const context = this;
        getObjectWithToken("users/profile")
            .then((response) => {
                if (response.data.error) {
                    //TODO: Handle error
                } else {
                    context.setState({
                        name: response.data.name,
                        email: response.data.email,
                        phone_number: response.data.phone_number.country_code + response.data.phone_number.number,
                    })

                }
            })
    }

    render() {
        return (
            <div id="contact_form" className="contact-form">
                <form onSubmit={this.handleSubmit}>
                    <Grid>
                        <div className="section-title__holder">
                            <div className="section-title">
                                <h3>{this.props.strings.contactForm}</h3>
                            </div>
                        </div>
                        <div className='contact-form-input'>
                            <Row>
                                <Col md={6}>
                                    <FormGroup controlId="formValidationNull">
                                        <ControlLabel>{this.props.strings.name}<sup>*</sup>{" "}</ControlLabel>
                                        <FormControl placeholder={this.props.strings.name}
                                            type="text"
                                            name="name"
                                            value={this.state.name}
                                            onChange={(e) => this.changeValue(e, "name")} />
                                    </FormGroup>
                                    <FormGroup controlId="formValidationNull">
                                        <ControlLabel>{this.props.strings.email}<sup>*</sup>{" "}</ControlLabel>
                                        <FormControl placeholder={this.props.strings.email}
                                            type="email"
                                            onChange={(e) => this.changeValue(e, "email")}
                                            name="email"
                                            value={this.state.email} />
                                    </FormGroup>
                                    <FormGroup controlId="formValidationNull">
                                        <ControlLabel>{this.props.strings.phoneNumber}<sup>*</sup>{" "}</ControlLabel>
                                        <FormControl placeholder={this.props.strings.phoneNumber}
                                            type="text"
                                            name="telephone"
                                            onChange={(e) => this.changeValue(e, "phone_number")}
                                            value={this.state.phone_number} />
                                    </FormGroup>
                                    <FormGroup controlId="formValidationNull">
                                        {console.log(sessionStorage.getItem("lang"))}
                                        <ControlLabel for="document-type">{this.props.strings.subject}<sup>*</sup></ControlLabel>
                                        {sessionStorage.getItem("lang") === 'en' ?
                                        <DropdownButton
                                            title={this.state.title}
                                            id="document-type"
                                            onSelect={this.handleSelect.bind(this)}
                                        >
                                           
                                            {optionsEn.map((subject, i) => (
                                                <MenuItem key={i} eventKey={i} >
                                                    {subject}
                                                </MenuItem>
                                            ))}
                                        </DropdownButton> : <DropdownButton
                                            title={this.state.title}
                                            id="document-type"
                                            onSelect={this.handleSelect.bind(this)}
                                        >
                                           
                                            {optionsAr.map((subject, i) => (
                                                <MenuItem key={i} eventKey={i} >
                                                    {subject}
                                                </MenuItem>
                                            ))}
                                        </DropdownButton> }
                                    </FormGroup>
                                </Col>
                                <Col md={6}>
                                    <FormGroup controlId="formValidationNull">
                                        <ControlLabel>{this.props.strings.message}<sup>*</sup></ControlLabel>
                                        <FormControl
                                            rows={9}
                                            cols={50}
                                            componentClass="textarea"
                                            placeholder={this.props.strings.writeMsg}
                                            onChange={(e) => this.changeValue(e, "message")}
                                            name="description"
                                            value={this.state.message}
                                        />
                                    </FormGroup>


                                    <div className="contact__button-submit">
                                        <Button
                                            type="submit"
                                            className="contact-form__btn-submit btn btn-default"
                                        >
                                            {this.props.strings.submitForm}
                                        </Button>
                                    </div>
                                </Col>
                            </Row>

                        </div>
                    </Grid>

                </form>
            </div>
        );
    }
}

export default ContactForm;

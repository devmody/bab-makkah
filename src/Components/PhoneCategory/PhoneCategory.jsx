import React from "react";
import { Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, NavItem, Navbar, Carousel, Tabs, Tab } from 'react-bootstrap'
import ProductItem from "../ProductItem/ProductItem";
import { Link } from 'react-router-dom'
import { getAllObjects, toCloudinary } from "../../services/CommonServices";
class PhoneCategory extends React.Component {
    state = {
        products: []
    }
    componentDidMount() {
        getAllObjects('product/homepage', { category: this.props.category._id })
            .then((response) => {
                this.setState({ products: response.data })
            })
            .catch((err) => {
                console.log(err)
                console.log(err.response)
            })
    }
    render() {
        const category = this.props.category
        return (
            <div className="PhoneCategory">
                <div className="section-title__holder">
                    <div className="section-title">
                        <Link to={'/shop?category=' + category._id}>
                            <h3>
                                {category.name[sessionStorage.getItem("lang")]}

                            </h3>
                        </Link>
                    </div>
                </div>
                <div className="category-1">
                    <Row style={{ padding: '0px 15px' }}>
                        <Col md={6}>
                            <div className="PhoneCategory_left">
                                <Link to={'/shop?category=' + category._id}>
                                    <img src={toCloudinary(category.featured_image, 376, 571) || require("../../img/Bab Makkah logo.png")}
                                        onError={e => {
                                            e.target.src = require("../../img/Bab Makkah logo.png");
                                        }} />
                                </Link>
                            </div>
                        </Col>
                        {
                            this.state.products.slice(0, 2).map((product) => (
                                <Col className="grey-bordered" md={3}>
                                    <ProductItem product={product} />
                                </Col>
                            ))
                        }
                    </Row>
                    <Row style={{ padding: '0px 15px' }}>
                        {
                            this.state.products.slice(2, 6).map((product) => (
                                <Col className="grey-bordered" md={3}>
                                    <ProductItem product={product} />
                                </Col>
                            ))
                        }
                    </Row>
                </div>
            </div>
        );
    }
}
export default PhoneCategory;
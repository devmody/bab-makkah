import React from "react";
import { Pagination, Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, NavItem, Navbar, Carousel, Tabs, Tab } from 'react-bootstrap'
import ReactTooltip from 'react-tooltip'
import ProductItem from "../ProductItem/ProductItem";
import Spinner from 'react-spinkit'
import './GridProduct.css'
class Shop extends React.PureComponent {
  handleNext() {
    let productsLength = this.props.products.length;
    if (this.props.currentPage < (productsLength / this.props.limit) - 1) {
      this.props.onPageChange(this.props.currentPage + 1)
    }
  }
  handlePrevious() {
    if (this.props.currentPage > 0) {
      this.props.onPageChange(this.props.currentPage - 1)
    }
  }
  handlePage(eventKey) {
    this.props.onPageChange(eventKey.target.id * 1)
  }
  render() {
    let pages = [];
    let productsLength = this.props.totalCount;
    for (let number = 0; number < productsLength / this.props.limit; number++) {
      pages.push(
        <Pagination.Item active={number + "" === this.props.currentPage + ""} id={number} onClick={this.handlePage.bind(this)}>{number + 1}</Pagination.Item>
      );
    }
    return (
      <div className="shop-grid">

        {!this.props.ready ? (<Spinner style={{ height: 1587 }} fadeIn={0} color="#2F2E33" name="rotating-plane" />) : (
          <Row>
            {
              this.props.products.map((product) => (
                <Col md={4} className="border-item">
                  <ProductItem product={product} />
                </Col>
              ))
            }
            {
              this.props.products.length === 0 ? (
                <Col md={12} style={{ height: 1587, textAlign: 'center' }}>
                  <h4>No results found</h4>
                </Col>
              ) : (
                  <Col md={12} style={{ textAlign: 'center' }}>

                    < Pagination >
                      <Pagination.Prev onClick={this.handlePrevious.bind(this)} />
                      {
                        pages
                      }
                      <Pagination.Next onClick={this.handleNext.bind(this)} />
                    </Pagination>
                  </Col>
                )
            }
          </Row>
        )}
      </div>
    );
  }
}
export default Shop;
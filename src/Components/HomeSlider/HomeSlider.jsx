import React from "react";
import { Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, NavItem, Navbar, Carousel } from 'react-bootstrap'
import ProductItem from "../ProductItem/ProductItem";
import { getSingleObject } from "../../services/CommonServices";
import './HomeSlider.css'
class HomeSlider extends React.Component {
  state = {
    featuredProduct: null
  }
  componentDidMount() {
    getSingleObject('product/single-featured', '')
      .then((response) => {
        this.setState({ featuredProduct: response.data })
      })
      .catch((err) => {
        console.log(err.response)
      })
  }
  render() {
    return (
      <div className="HomeSlider">
        <Row>
          <Col md={9}>
            <div className="HomeSlider-left">
              <Carousel indicators={false}>
                <Carousel.Item>
                  {
                    this.props.bannerImage?(
                      <img width={900} height={450} alt="900x500" src={this.props.bannerImage} />
                    ):('')
                  }
                </Carousel.Item>
              </Carousel>
            </div>
          </Col>
          <Col md={3}>
            <div className="grey-bordered">
              <ProductItem
                product={this.state.featuredProduct}
              />
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default HomeSlider;



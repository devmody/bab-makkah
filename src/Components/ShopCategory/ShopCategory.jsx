import React from "react";
import { Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, NavItem, Navbar } from 'react-bootstrap'
import { withRouter } from 'react-router-dom'
import './ShopCategory.css'
class ShopCategory extends React.Component {
	constructor(props) {
			super(props);
	}

	render() {
		return (
			<div className="Shopcategory">
				<Navbar>
					<Nav>
						<div className="Shopcategory__menu">
							<ButtonToolbar>
								<DropdownButton
									bsSize="large"
									title={this.props.strings.shopCategory}
									id="dropdown-size-large"
								>
									{
										this.props.categories.map((category, key) => (
											<MenuItem onClick={() => this.props.history.push('/shop?category=' + category._id)} eventKey={key}>
												{category.name[sessionStorage.getItem("lang")]}</MenuItem>
										))
									}
								</DropdownButton>
							</ButtonToolbar>
						</div>
						{/* <NavItem className="special" eventKey={1} href="#">
							Trending products
								</NavItem>
						<NavItem eventKey={2} href="#">
							<div class="Shopcategory__special-mark">
								<p>Hot</p>
							</div>
							Today Deals
								</NavItem>
						<NavItem eventKey={3} href="#">
							<div class="Shopcategory__special-mark green">
								<p>New</p>
							</div>
							Just Arrived
								</NavItem>
						<NavItem eventKey={4} href="#">
							Blog
								</NavItem>
						<NavItem eventKey={5} href="#">
							Contact
								</NavItem> */}
					</Nav>
				</Navbar>
			</div>
		);
	}
}

export default withRouter(ShopCategory);



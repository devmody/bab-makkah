import React from "react";
import InputRange from 'react-input-range';
import './ShopFilter.css'
import { withRouter } from 'react-router-dom'
import queryString from 'query-string'
import { Collapse, Button, ToggleButtonGroup, ToggleButton } from 'react-bootstrap'
class Filter extends React.Component {
    constructor(props) {
        super(props);
        this.handleFilterClick = this.handleFilterClick.bind(this)
        this.state = {
            value: { min: 0, max: 10 },
            open: window.screen.width <= 992 ? false : true,
            icon: true,
            iconName: '',
            selectedcategory: ''
        };
    }
    // getSubCategories(category){
    //     category = this.state.selectedcategory;
    //     const query = queryString.parse(this.props.location.search)
    //     let types = []
    //     this.props.types.map((type) => {
    //         type.category._id === query.category?  types.push(type) : ''
    //     })
    // }
    handleFilterClick(e, value) {
        console.log('type', value);
        console.log('e.target.name', e.target.name);
        let query = queryString.parse(this.props.location.search);
        console.log('query first' , query);
        if (e.target.name === "vendor" ) {
            delete query[e.target.name];
            this.props.history.push({
                pathname: "shop",
                search: queryString.stringify(Object.assign(query, { page: 0, [e.target.name]: value }))
            })
        } else if (query[e.target.name] === value)  {
           // console.log('selectedCategory value', this.state.selectedcategory);
            //this.setState({selectedcategory: value});
            delete query[e.target.name]
            console.log('query value', query);
            this.props.history.push({
                pathname: "shop",
                search: queryString.stringify(query)
            })
        } 
        else {
            console.log('queryelse', query);
            if(e.target.name === 'category'){
                console.log('category');
                this.setState({selectedcategory: value});
            }
            
            console.log('selectedCategory else', this.state.selectedcategory);
            this.props.history.push({
                pathname: "shop",
                search: queryString.stringify(({ page: 0, [e.target.name]: value }))
            })
        }
        console.log('query last', query);
    }
    
    render() {
        //console.log('types', this.props.types);
        const query = queryString.parse(this.props.location.search)
        const { open } = this.state;
        const { icon } = this.state;
        return (
            <div className="Filter">
                <div className="section-title__holder">
                    <div className="section-title">
                        <h3>{this.props.strings.filter}</h3>
                    </div>
                    <Button
                        onClick={() => this.setState({ open: !open, icon: !icon })}
                        aria-controls="example-collapse-text"
                        aria-expanded={open}
                        className="shop-filter-arrow"
                    >
                        {
                            this.state.icon ?


                                <i class="fa fa-angle-down"></i> : <i class="fa fa-angle-up"></i>
                        }

                    </Button>
                </div>
                <div className="Filter-slider">
                    <h4>Price </h4>
                    <p>Price : $12 - $120 </p>
                    <InputRange
                        maxValue={20}
                        minValue={0}
                        value={this.state.value}
                        onChange={value => this.setState({ value })} />
                    <button type="button" class="btn btn-primary">Filter</button>
                </div>
                <Collapse in={this.state.open}>
                    <div id="example-collapse-text">
                    
                        <div className="Filter-by">
                            <h4>{this.props.strings.category}</h4>
                            {
                                this.props.categories.map((category,key) => (
                                    <div className="Filter-by__item">
                                        <input name="category" checked={ query.category === category._id } onClick={(e) => this.handleFilterClick(e, category._id,key)} type="checkbox" id="Filter-by" /> <span>{category.name[sessionStorage.getItem("lang")]}</span>
                                    </div>
                                ))
                            }
                        </div>
                       
                        {
                            this.state.selectedcategory  ? (
                                <div className="Filter-by">
                                    <h4>{this.props.strings.type} </h4>
                                    {    
                                        this.props.types.map((type) => (
                                            type.category._id === this.state.selectedcategory ?

                                            <div className="Filter-by__item">
                                               
                                                <input name="type" checked={query.type === type._id} onClick={(e) => this.handleFilterClick(e, type._id)} type="checkbox" id="Filter-by" /> <span>{type.name[sessionStorage.getItem("lang")]}</span>
                                            </div>  : ('')
                                            
                                            
                                        ))
                                    }
                                </div>
                            ) : ('')
                        }
                       <div className="Filter-by">
                            <h4>{this.props.strings.brand}</h4>
                            {
                                this.props.brands.map((brand) => (
                                    <div className="Filter-by__item">
                                        <input name="brand_ref" checked={query.brand_ref === brand._id} onClick={(e) => this.handleFilterClick(e, brand._id)} type="checkbox" id="Filter-by" /> <span>{brand.name[sessionStorage.getItem("lang")]}</span>
                                    </div>
                                ))
                            }
                        </div>
                    </div>

                </Collapse>
            </div>
        );
    }
}
export default withRouter(Filter);
import React from "react";
import { Row, Col, Grid, Button, DropdownButton, MenuItem, Breadcrumb } from 'react-bootstrap'
import { Link,withRouter } from 'react-router-dom'
import queryString from 'query-string'

class Breadcrumbs extends React.Component {
    render() {
        const query = queryString.parse(this.props.location.search)
        return (
            <div>
                <div className="breadcrumb-holder">
                    <div className="container">
                        <Breadcrumb>
                            <Breadcrumb.Item><Link to="/">Home</Link></Breadcrumb.Item>
                            <Breadcrumb.Item><Link to={"/shop?category="+query.category}>Category</Link></Breadcrumb.Item>
                            {query.type?(<Breadcrumb.Item><Link to={"/shop?category="+query.category+"&type="+query.type}>Subcategory</Link></Breadcrumb.Item>):('')}
                            {/* <Breadcrumb.Item><Link >Product</Link></Breadcrumb.Item> */}
                        </Breadcrumb>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(Breadcrumbs);



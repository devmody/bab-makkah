import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { postObjectWithToken } from '../../services/CommonServices'
import { Button } from 'react-bootstrap'
class PaymentValidation extends Component {
    state = {
        message: "Loading..."
    }
    componentDidMount() {
        postObjectWithToken('order/after-pay-page').then((response) => {
            this.setState({
                message: response.data.result
            })
        }).catch((err) => {
            console.log(err)
            console.log(err.response)
            this.props.history.push('/')
        })
    }
    render() {
        if (!localStorage.getItem("token")) {
            this.props.history.push('/login')
        }
        return (
            <div className="validation">
                <h2>
                    {this.state.message}
                </h2>
                <h4>
                    <Link to="/shop">
                        <Button className="validation-btn__shop">Back to shop</Button>
                    </Link>
                    <Link to="/account-orders">
                        <Button className="validation-btn__orders">Your orders</Button>
                    </Link>
                </h4>
            </div>
        );
    }
}

export default withRouter(PaymentValidation);
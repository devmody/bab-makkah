import React from "react";
import { Row, DropdownButton, MenuItem, ToggleButton, ToggleButtonGroup } from 'react-bootstrap'
import { withRouter } from 'react-router-dom'
import queryString from 'query-string'

const filterToTitle = {
  "": "bestMatch",
  "name.en=1": "aToZ",
  "name.ar=1": "aToZ",
  "name.en=-1": "zToA",
  "name.ar=-1": "zToA",
  "sizes.price=1": "lowToHigh",
  "sizes.price=-1": "highToLow",
}
class ShopSort extends React.Component {
  state = {
    title: "Best Match"
  }
  handleSort = (eventKey) => {
    let query = queryString.parse(this.props.location.search)
    this.props.history.push({
      pathname: "shop",
      search: queryString.stringify(Object.assign(query, { page: 0, sortBy: queryString.stringify(eventKey) }))
    })
  }
  componentDidMount() {
    this.setState({ title: this.props.strings[filterToTitle[queryString.parse(this.props.location.search).sortBy || ""]] })
  }

  componentWillReceiveProps() {
    this.componentDidMount()
  }
  render() {
    function calculateFromAndTo(page, limit, count) {
      const from = (page * limit) + limit
      const to = (from - limit) + 1
      return to +" - "+ Math.min(from,count)
    }

    function calculateTo(page, limit) {

    }
    return (
      <div className="Shopsort clearfix">
        <div className="Shopsort_left">
          <Row style={{ borderBottom: '1px solid #ECECE7', paddingBottom: 4 }}>
            <span><h3> {this.props.strings.sort} : </h3> </span>
            <span>
              <DropdownButton
                bsSize="xsmall"
                title={this.state.title}
                onSelect={this.handleSort}
                id="dropdown-size-extra-small">
                <MenuItem eventKey={{}}>{this.props.strings.bestMatch}</MenuItem>
                <MenuItem eventKey={{ "sizes.price": 1 }}>{this.props.strings.lowToHigh}</MenuItem>
                <MenuItem eventKey={{ "sizes.price": -1 }}>{this.props.strings.highToLow}</MenuItem>
                <MenuItem eventKey={{ ["name." + sessionStorage.getItem("lang")]: 1 }}>{this.props.strings.aToZ}</MenuItem>
                <MenuItem eventKey={{ ["name." + sessionStorage.getItem("lang")]: -1 }}>{this.props.strings.zToA}</MenuItem>
              </DropdownButton>
            </span>
            <div className="Shopsort__result">
              <span> <em class="shopsort__result__count">{calculateFromAndTo(this.props.currentPage,this.props.limit,this.props.totalCount)}</em> {this.props.strings.of} <em class="shopsort__result">{this.props.totalCount}</em> {this.props.strings.results} </span>
              <span>
                <ToggleButtonGroup type="radio" name="options" defaultValue={1} className="Shopsort__grid-list-selector">
                  <ToggleButton value={1}><i class="fa fa-th"></i></ToggleButton>
                  <ToggleButton value={2}><i class="fa fa-list"></i></ToggleButton>
                </ToggleButtonGroup>
              </span>
            </div>
          </Row>
        </div>
      </div>
    );
  }
}
export default withRouter(ShopSort);
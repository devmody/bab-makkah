import React from "react";
import { Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, NavItem, Navbar, Carousel } from 'react-bootstrap'
import { tempProduct } from '../../services/TempObjects'
import { Link } from 'react-router-dom'
import { toPrice,toCloudinary} from '../../services/CommonServices'
class BestProductItem extends React.Component {

    render() {
        const product = this.props.product ? (this.props.product) : (tempProduct)
        return (
            <div className="pro-item">
                <div className="product-wrapper">
                    <div className="product-img">
                        <Link to={"product/" + product._id}>
                            <img src={product.images[0] ? (toCloudinary(product.images[0], null, 90)) : require("../../img/Bab Makkah logo.png")}
                                onError={e => {
                                    e.target.src = require("../../img/Bab Makkah logo.png");
                                }}
                            />
                            {/* <img className="secondary-img" src={require("../../img/phone2.png")} /> */}
                        </Link>
                    </div>
                    <div className="product-content pt-15">
                        <Link to={"product/" + product._id}>
                            <h2>
                                {product.name[sessionStorage.getItem("lang")]}
                            </h2>
                        </Link>
                        <div className="product-meta">
                            <div className="pro-price f-left">
                                <span>{toPrice(product.sizes[0].price)} SAR</span>
                                {/* <span className="old-price">$79.00</span> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}
export default BestProductItem;
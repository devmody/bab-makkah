import React from "react";
import {
  Row,
  Col,
  Grid,
  Button,
  DropdownButton,
  MenuItem,
  ButtonToolbar,
  NavDropdown,
  Nav,
  NavItem,
  Navbar,
  Carousel,
  Tabs,
  Tab
} from "react-bootstrap";
import { toCloudinary } from '../../services/CommonServices'
import { Link } from 'react-router-dom'
class FeatuedCategoris extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      featuredCategories: [
        {
          en: 'Packed Dates',
          ar: 'تمور معلبة',
          url: '/shop?category=5be4001f50cfe60cfb77ea65&type=5bf2c3b469049645c02991f2',
          img: "../../img/cat2.png"
        },
        {
          en: 'Oud',
          ar: 'عود',
          url: '/shop?category=5bf2bf2169049645c02991e2&type=5bf2c6ff69049645c02991ff'
        },
        {
          en: 'Oriental Fragrances',
          ar: 'عطور شرقية',
          url: '/shop?category=5bf2bf2169049645c02991e2&type=5bf2c74669049645c0299201'
        },
        {
          en: 'Carpets', ar: 'سجاد', url: '/shop?category=5bf2bf0069049645c02991e0'
        }
      ],
    };
  }

  render() {
    const types = this.props.types
    return (
      <div className="FeatuedCategoris">
        <div className="container">
          <div className="featured-section__title">
            <h3> {this.props.strings.featuredCategories}</h3>
          </div>
          <div className="row Featued-layout-promo-box">
            {
              types.map((type) => (
                <div className="col-sm-6">
                  <Link
                    to={'/shop?category=' + type.category._id + '&type=' + type._id}
                    className="Featued-promo-box Featued-one-child hover-type-2"
                  >
                    <img src={toCloudinary(type.featured_image, 320, 555) || require("../../img/Bab Makkah logo.png")} alt={type.name[sessionStorage.getItem("lang")]}
                      onError={e => {
                        e.target.src = require("../../img/Bab Makkah logo.png");
                      }}
                    />
                    <div className="Featued-description">
                      <div className="Featued-description-wrapper">
                        {/* <div className="Featued-background" />
                         <div className="Featued-title-small">
                          {type.name[sessionStorage.getItem("lang")]}
                        </div>  */}
                      </div>
                    </div>
                  </Link>
                </div>
              ))
            }
          </div>
        </div>
      </div>
    );
  }
}
export default FeatuedCategoris;

import React from "react";
import { Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, NavItem, Navbar, ToggleButton, ToggleButtonGroup } from 'react-bootstrap'
import { Carousel } from 'react-responsive-carousel';
import InputNumber from 'rc-input-number';
import 'rc-input-number/assets/index.css';

import Breadcrumbs from "../Breadcrumbs/Breadcrumbs";
import ShopCategory from "../ShopCategory/ShopCategory";
import { getSingleObject, toPrice, updateObjectWithToken, calculateRatingAverage, toCloudinary, addToLocalCart } from "../../services/CommonServices";
import { tempProduct } from '../../services/TempObjects'
import './SingleShop.css'
import ReactGA from 'react-ga'
import StarRatingComponent from "react-star-rating-component";
import { toast, Flip } from 'react-toastify';
import { Link, withRouter } from 'react-router-dom'
import CartContainer from "../../Cart";
import { Subscribe } from "unstated";
import { get, groupBy } from 'lodash';

class SingleShop extends React.Component {
    toastSettings = {
        position: "top-right",
        autoClose: 5000,
        transition: Flip,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        className: "sub-toast"
    }
    constructor() {
        super()
        this.handleCart = this.handleCart.bind(this)
        this.getTotal = this.getTotal.bind(this)
        this.state = {
            selectedSize: 0,
            selectedColor: null,
            reviews: [],
            filteredColors: [],
            groupedSizes: [],
            selectedSizeText: '',
            quantity: 1,
            total: 0
            
        }
    }
    
    handleCart(e, cart) {
        console.log('cart', cart);
        e.preventDefault();
        let p = {
            product: this.state.product._id,
            size: this.state.selectedSize,
            color: this.state.selectedColor,
            quantity: this.state.quantity ? this.state.quantity : 1
        };
        if (localStorage.getItem("token")) {
            updateObjectWithToken("users/cart", p).then(response => {
                cart.refreshCart()
                if (response.data.message === "warning")
                    toast.warning(
                        this.props.strings.cartError, this.toastSettings
                    );
                else
                    toast.success(
                        this.props.strings.cartSuccess, this.toastSettings
                    );

            });
        }

        else {
            console.log('no token');
            console.log('product',this.state.product );
            addToLocalCart({
                product: this.state.product,
                size: this.state.selectedSize,
                color: this.state.selectedColor,
                quantity: this.state.quantity ? this.state.quantity : 1
            }, cart.refreshCart)

            toast.success(
                this.props.strings.cartSuccess, this.toastSettings
            );
           
           
        }
    }
    getTotal() {
        let totalPrices = 0;
       // console.log('quantity', this.state.quantity);
        totalPrices += this.state.product.sizes[this.state.selectedSize].price * this.state.quantity
        //console.log('totalPrices', totalPrices)
        this.setState({ total: totalPrices.toFixed(2) })
    }
    // changeQuantity( n ) {
	// 	let shopProduct = this.state.product;
	// 	shopProduct.quantity += n;
    //     shopProduct.quantity = (shopProduct.quantity > 10) ? 10 : shopProduct.quantity;
    //     console.log('productQuantity',shopProduct.quantity );
    //     this.setState({quantity: shopProduct.quantity});
    //     console.log('quantity', this.state.quantity);
	// 	this.getTotal();
	// }

	// handleQuantityChange(value) {
	// 	const quantity = value;
	// 	if (quantity)
	// 		this.changeQuantity( quantity - this.state.product.quantity);
	// }
   async handleQuantityChange(quantity) {
        // console.log('e', n);
        // let shopProduct = this.state.product;
        // console.log('shopProduct', shopProduct);
        // shopProduct.quantity += n;
        // let quan = 0;
        // quan += n;
        //shopProduct.quantity = (shopProduct.quantity > 10) ? 10 : shopProduct.quantity;
        quantity = (quantity > 10) ? 10 : quantity;
        //this.setState({ product: shopProduct });
        await this.setState({ quantity: quantity })
        this.getTotal();
        //console.log('shopProduct.quantity',shopProduct.quantity);
    }


    handleCheck = (size) => {
        this.setState({ selectedSizeText: size })
    }
    componentDidMount() {
        window.scroll(0, 0)
        getSingleObject('product', this.props.match.params.id)
            .then(async (response) => {
                this.setState({ product: response.data, ready: true })
                //console.log('response.data', response.data)
                const filteredColors = groupBy(response.data.sizes, "name.en")
                await this.setState({ filteredColors: filteredColors, groupedSizes: Object.keys(filteredColors).map(size => size) })
            })
    }
    countReviewers = () => {
        var count = 0;


        for (var i = 0; i < get(this.state.product, "reviews.length", 0); i++) {
            count++;
        }
        return count

    }
    handleMouseOver = () => {
        toast.warning(
            'no stock', this.toastSettings
        )
    }
    handleMouseOverAddToCart = () => {
        toast.warning(
            'Please Select Size and Color if exist', this.toastSettings
        )
    }



    addToWishlist() {
        if (!localStorage.getItem("token")) {
            this.props.history.push({ pathname: "/login" })
            return;
        }
        updateObjectWithToken('users/wishlist', { product: this.props.match.params.id })
            .then((response) => {
                toast.success(this.props.strings.wishlistSuccess, this.toastSettings)
            })
            .catch((error) => {
                toast.error(this.props.strings.wishlistError, this.toastSettings)
            })

    }

    componentDidUpdate() {
        window.scroll(0, 0)

    }
    render() {
        const product = this.state.product ? (this.state.product) : (tempProduct)
        //console.log('product', product);
        const starRatingProps = {
            starCount: 5,
            renderStarIcon: () => (
                <span>
                    {" "}
                    <i className="fa fa-star" />{" "}
                </span>
            ),
            emptyStarColor: "#dddddd",
        };
        const avgRating = calculateRatingAverage(
            product.reviews
        ).toFixed(1);

        return (
            <div>
                {/* <Breadcrumbs /> */}
                <div className="single-shop">
                    <Grid>
                        <Col className="carousel-col" style={{
                            background: 'white',
                            height: 637
                        }} md={5}>
                            {
                                this.state.ready ?
                                    <Carousel
                                        // style={{margin:-1}}
                                        showIndicators={false}
                                        showStatus={false}
                                        autoPlay={true}
                                        showArrows={false}
                                        showStatus={false}
                                    >
                                        {
                                            product.images.map((image) => (
                                                <div style={{ margin: 1 }}>
                                                    <img src={toCloudinary(image, null, 443) || require("../../img/Bab Makkah logo.png")}
                                                        onError={e => {
                                                            e.target.src = require("../../img/Bab Makkah logo.png");
                                                        }}
                                                    />
                                                </div>
                                            ))
                                        }
                                        {
                                            product.images.length === 0 ? (
                                                <img src={require("../../img/Bab Makkah logo.png")}
                                                    onError={e => {
                                                        e.target.src = require("../../img/Bab Makkah logo.png");
                                                    }}
                                                />
                                            ) : ([])
                                        }
                                    </Carousel>
                                    :
                                    <div />
                            }
                        </Col>
                        <Col className="single-shop__details" md={7}>
                            <div className="single-shop__name">
                                <h3>{product.name[sessionStorage.getItem("lang")]}</h3>
                            </div>
                            {product.reviews.length > 0 ?
                                (
                                    <div>
                                        <StarRatingComponent
                                            name="rate1"
                                            {...starRatingProps}
                                            value={avgRating}
                                            editing={false}
                                        />

                                        <span style={{ marginLeft: "10px", position: "absolute" }}>({this.countReviewers()})</span>
                                    </div>
                                ) : ('')
                            }
                                
                            <div className="single-shop__rating">
                                {
                                    product.brand_ref ?
                                        <h3>{this.props.strings.brand}: {product.brand_ref.name[sessionStorage.getItem("lang")]}</h3>
                                        :
                                        ''
                                }
                                {/* {
                                    product.vendor ?
                                        <h3>{this.props.strings.vendor}: {product.vendor.name[sessionStorage.getItem("lang")]}</h3>
                                        :
                                        ""
                                } */}
                            </div>
                            <div className="pro-price f-left">
                                <span className="new-price" >{toPrice(product.sizes[this.state.selectedSize].price)} SAR</span>
                                {/* <span className="old-price" >$79.00</span> */}
                            </div>
                            <div >
                                <h4> {this.props.strings.quantity} </h4>
                                <div style={{ margin: 5 }}>
                                    <InputNumber
                                        min={1} 
                                        max={10}
                                        step={1}
                                        value={this.state.quantity}
                                        onChange={(e) => this.handleQuantityChange(e)}
                                    /> 
                                 </div>
                               
                            </div>
                            <div className="pro-price f-left">
                                <h5  > {this.props.strings.total} </h5>
                                <span className="new-price" >{this.state.total} SAR </span>
                            </div>
                            <div className="single-shop__state">  <em className="single-shop__availvale">{this.props.strings.inStock} </em>
                            </div>
                            {console.log('pro', product)}
                            {product.colors && product.colors.length > 0 ? (
                                <div className="single-shopــcolor">
                                    <h4> {this.props.strings.color}</h4>
                                    <ButtonToolbar>
                                        <ToggleButtonGroup onChange={(colorId) => this.setState({ selectedColor: colorId })} type="radio" name="options" value={this.state.selectedColor}>
                                            {
                                                product.colors.map((color, key) => (
                                                    <ToggleButton value={color._id}>{color.name[sessionStorage.getItem("lang")]}</ToggleButton>
                                                ))
                                            }
                                        </ToggleButtonGroup>
                                    </ButtonToolbar>
                                </div>
                            ) : ('')}
                            <div className="single-shopــstorge">
                                <h4>{this.props.strings.size} ({product.sizesUnit[sessionStorage.getItem("lang")]})</h4>

                                {Object.keys(this.state.filteredColors).map((size, key) => (
                                    // <ButtonToolbar>

                                    <ToggleButtonGroup onChange={(key) => this.setState({ selectedSize: key, selectedColor:null })} onClick={() => this.handleCheck(size)} type="radio" name="options" value={this.state.selectedSize}>
                                        <ToggleButton value={key}>{size}</ToggleButton>
                                    </ToggleButtonGroup>

                                    // </ButtonToolbar>

                                ))}
                                <br /> <br />
                                {console.log('filtercolors', this.state.filteredColors)}
                                {
                                    this.state.selectedSizeText ?
                                    
                                        this.state.filteredColors[this.state.selectedSizeText].map((color, key) => (
                                            // <ButtonToolbar>
                                            get(color.colors, "_id", "") ?
                                                <ToggleButtonGroup onChange={(colorId) => this.setState({ selectedColor: colorId })} type="radio" name="options" value={this.state.selectedColor}>
                                                    <ToggleButton disabled={color.stock === 0 ? true: false} 
                                                                  value={get(color.colors, "_id", 0)}
                                                                  onMouseOver={color.stock === 0 ? this.handleMouseOver: ''}>
                                                                  {color.colors.name[sessionStorage.getItem("lang")]}
                                                    </ToggleButton>
                                                </ToggleButtonGroup>
                                                : <div>{sessionStorage.getItem("lang") === "en" ? "NO COLOR" : "لا يوجد لون"}</div>
                                            // </ButtonToolbar>

                                        ))
                                        : null
                                }

                                <ButtonToolbar>
                                    <ToggleButtonGroup onChange={(key) => this.setState({ selectedSize: key })} type="radio" name="options" value={this.state.selectedSize}>
                                        {
                                            product.sizes.colors ?
                                            
                                                product.sizes.colors.map((color, key) => (
                                                    <ToggleButton value={key}>{ color.name[sessionStorage.getItem("lang")] }</ToggleButton>
                                                ))
                                                : null}
                                    </ToggleButtonGroup>
                                </ButtonToolbar>

                            </div>
                            
                            <div className="single-shop__addcart">
                                {console.log('selectedcolor', this.state.selectedColor)}
                                <Subscribe to={[CartContainer]}>
                                   
                                    {cart => (
                                        <span> 
                                            
                                            <Button  disabled={product.sizes[0].colors ? this.state.filteredColors[this.state.selectedSizeText] ? this.state.selectedColor? false: true :true : false}
                                                     onClick={(e) => this.handleCart(e, cart)}
                                                     onMouseOver={ this.state.selectedSizeText? this.state.selectedColor? ' ' : this.handleMouseOverAddToCart: this.handleMouseOverAddToCart}>
                                                     {this.props.strings.addToCart}
                                            </Button> 
                                         </span>
                                    )}
                                </Subscribe>
                                <span> <Button onClick={this.addToWishlist.bind(this)} className="wishlist-btn"><i class="material-icons">favorite</i> {this.props.strings.addToWishlist}</Button> </span>
                            </div>
                            <div className="single-shop__Description clearfix">
                                <h4> {this.props.strings.description} </h4>
                                <h3>{product.description[sessionStorage.getItem("lang")] ? (product.description[sessionStorage.getItem("lang")]) : this.props.strings.noDescription}</h3>
                            </div>
                            <hr />
                            <div className="single-shop__category">
                                <h3>
                                    <Row>
                                        <Col md={2}>
                                            {this.props.strings.sku}:
                                            </Col>
                                        <Col md={10}>
                                            {/* <em className="Categories-type"> &#x200E;{product.sizes[this.state.selectedSize].serial}</em> */}
                                            <em className="Categories-type"> &#x200E;{product.sku}</em>

                                        </Col>
                                    </Row>
                                </h3>
                            </div>
                            <div className="single-shop__category">
                                <h3>
                                    <Row>
                                        <Col md={2}>
                                            {this.props.strings.categories}:
                                            </Col>
                                        <Col md={10}>
                                            <Link to={"/shop?category=" + product.type.category._id + "&type=" + product.type._id}><em className="Categories-type"> {product.type.category.name[sessionStorage.getItem("lang")]}, {product.type.name[sessionStorage.getItem("lang")]}</em> </Link>                                           </Col>
                                    </Row>
                                </h3>
                            </div>
                            
                        </Col>
                    </Grid>
                </div>
            </div>
        );
    }
}
export default withRouter(SingleShop);
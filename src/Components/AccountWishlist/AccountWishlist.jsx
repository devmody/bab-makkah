import React from "react";
import { Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, NavItem, Navbar, Carousel, Modal, FormGroup, FormControl, ControlLabel } from 'react-bootstrap'
import ProductItem from "../ProductItem/ProductItem";
import AccountSidebar from "../AccountSidebar/AccountSidebar";
import AccountHeader from "../AccountHeader/AccountHeader";
import Breadcrumbs from "../Breadcrumbs/Breadcrumbs";
import { toast, Flip } from 'react-toastify';
import Spinner from 'react-spinkit'
import { getSingleObject, getObjectWithToken, toPrice, deleteObject, toCloudinary } from "../../services/CommonServices";
class AccountWishlist extends React.Component {
    toastSettings = {
        position: "top-right",
        autoClose: 5000,
        transition: Flip,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        ready: false,
        className: "sub-toast"
    }
    constructor(props, context) {
        super(props, context);
        this.handleHide = this.handleHide.bind(this);
        this.state = {
            show: false,
            products: []
        };
    }
    componentDidMount() {
        window.scroll(0, 0)
        getObjectWithToken('users/wishlist')
            .then((response) => {
                this.setState({ products: response.data.wishlist, ready: true })
            })
    }
    removeFromWishlist(e, key) {
        e.preventDefault()
        deleteObject('users/wishlist/', { _id: this.state.products[key]._id })
            .then((response) => {
                toast.success('Product removed', this.toastSettings)
                this.componentDidMount()
            })
            .catch((error) => {
                toast.error('An error has occured', this.toastSettings)
            })
    }
    handleHide() {
        this.setState({ show: false });
    }
    render() {
        return (
            <div className="account-wishlist">
                {/* <Breadcrumbs /> */}
                {/* <AccountHeader /> */}
                <Grid>
                    <Row>
                        <Col md={3}>
                            <AccountSidebar strings={this.props.strings} />
                        </Col>
                        <Col md={9}>
                            {/* <div className="account-wishlist__content">
              
            </div> */}
                            <div className="cart-border">

                                {
                                    this.state.ready ? (
                                        this.state.products.map((product, key) => (
                                            <Row>
                                                <Col md={4}>
                                                    <div className="cart-product__image">
                                                        <img src={toCloudinary(product.images[0], null, 248) || require("../../img/Bab Makkah logo.png")}
                                                            onError={e => {
                                                                e.target.src = require("../../img/Bab Makkah logo.png");
                                                            }}
                                                        />
                                                    </div>
                                                </Col>
                                                <Col md={6}>
                                                    <div className="cart-details">
                                                        <div className="cart-category">
                                                            <h4>{product.type.category.name[sessionStorage.getItem("lang")]}/{product.type.name[sessionStorage.getItem("lang")]}</h4>
                                                        </div>
                                                        <div className="cart-item">
                                                            <h4>{product.name[sessionStorage.getItem("lang")]}</h4>
                                                        </div>

                                                        <div className="pro-price f-left">
                                                            <span>{toPrice(product.sizes[0].price)} SAR</span>
                                                        </div>

                                                        <div className="single-shop__state">
                                                            <em className="single-shop__availvale">{this.props.strings.inStock}</em>

                                                        </div>
                                                        <div className="account-wishlit_btn">
                                                            <Button className="main-btn account-wishlit_btn">{this.props.strings.addToCart}</Button>
                                                        </div>

                                                    </div>
                                                </Col>
                                                <Col md={2}>
                                                    <div className="cart-delete">
                                                        <a style={{ cursor: 'pointer' }} onClick={(e) => this.removeFromWishlist(e, key)}>
                                                            <i class="material-icons">
                                                                delete
                                                    </i>
                                                        </a>
                                                    </div>
                                                </Col>
                                            </Row>
                                        ))
                                    ):
                                    (<Spinner style={{ height: 1587 }} fadeIn={0} color="#2F2E33" name="rotating-plane" />)
                                }
                                {
                                    this.state.products.length === 0 && this.state.ready ? (
                                        <h3>{this.props.strings.emptyWishlist}</h3>
                                    ) : ('')
                                }
                            </div>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}
export default AccountWishlist;
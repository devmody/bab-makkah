import React from "react";
import './PrivacyPolicy.css'
import { Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, ControlLabel, FormGroup, Navbar, Carousel, Tabs, Tab, FormControl } from 'react-bootstrap'
class PrivacyPolicy extends React.Component {
    constructor(props) {
        super(props)
    }
    componentDidMount() {
        window.scroll(0, 0)
    }
    render() {
        return (
            <div className={sessionStorage.getItem('lang') === "en" ? 'privacy-policy' : 'privacy-policy-ar'} >
                <Grid   >
                    <h3>{this.props.strings.PrivacyPolicy}</h3>
                    <p>{this.props.strings.PrivacyPolicyDesc} </p>
                    <h4>{this.props.strings.DataCollection} </h4>
                    <p>{this.props.strings.DataCollection1}</p>
                    <p>{this.props.strings.DataCollection2}</p>
                    <p>{this.props.strings.DataCollection3}</p>
                    <p>{this.props.strings.DataCollection4}</p>
                    <h4>{this.props.strings.DataUse} </h4>
                    <p>{this.props.strings.DataUseDesc}</p>
                    <p>{this.props.strings.DateUse1}</p>
                    <p>{this.props.strings.DateUse2}</p>
                    <p>{this.props.strings.DateUse3}</p>
                    <h4>{this.props.strings.ThirdPartyWebsites} </h4>
                    <p>{this.props.strings.ThirdPartyWebsitesDesc}  </p>
                </Grid>
            </div>
        );
    }
}
export default PrivacyPolicy;
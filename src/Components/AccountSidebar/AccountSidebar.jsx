import React from "react";
import { Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, NavItem, Navbar, Carousel } from 'react-bootstrap'
import ProductItem from "../ProductItem/ProductItem";
import { getSingleObject } from "../../services/CommonServices";
import { Link, withRouter } from 'react-router-dom'
class AccountSidebar extends React.PureComponent {

  render() {
    return (
      <div className="Account-sidebar">
        <div className="list-group">
          <Link to="account" className={"list-group-item" + (this.props.location.pathname === "/account" ? (" active") : (''))}><i className="first-icon fa fa-key"></i> <span>{this.props.strings.youraccount} </span> <i class="arrow-right material-icons">
            keyboard_arrow_right
            </i>
          </Link>
          <Link to="/account-wishlist" className={"list-group-item" + (this.props.location.pathname === "/account-wishlist" ? (" active") : (''))}><i className="first-icon fa fa-compass"></i> <span>{this.props.strings.wishlist}</span> <i class="arrow-right  material-icons">
            keyboard_arrow_right
            </i>
          </Link>
          <Link to="/account-orders" className={"list-group-item" + (this.props.location.pathname === "/account-orders" ? (" active") : (''))}><i className="first-icon fa fa-compass"></i> <span>{this.props.strings.yourorders}</span> <i class="arrow-right  material-icons">
            keyboard_arrow_right
            </i>
          </Link>
          <Link to="/account-loyalty" className={"list-group-item" + (this.props.location.pathname === "/account-loyalty" ? (" active") : (''))}><i className="first-icon fa fa-compass"></i> <span>{this.props.strings.loyaltyProgram}</span> <i class="arrow-right  material-icons">
            keyboard_arrow_right
            </i>
          </Link>
        </div>
      </div>
    );
  }
}

export default withRouter(AccountSidebar);





import React from "react";

import { Row, Col, Grid, Button, DropdownButton, MenuItem, ButtonToolbar, NavDropdown, Nav, NavItem, Navbar, Carousel } from 'react-bootstrap'

class DeliveryProcess extends React.Component {
  render() {
    return (
      <div className="Delivery-area ">

        <div className="container">
          <div className="section-title__holder">
            <div className="section-title">
              <h3> Process Delivery</h3>
            </div>
          </div>
          <div className="row">
            <Col md={2} className="custome-width">
              <div className="Delivery-wrapper ">
                <i class="material-icons">
                  add_location
                    </i>
                <div className="Delivery-content">
                  <h3>Free Shipping</h3>
                  <p>Nulla vitae elit libero, a phare augue cum sociis natoque.</p>
                </div>
              </div>
            </Col>
            <Col md={2} className="custome-width">
              <div className="Delivery-wrapper ">
                <i class="material-icons">
                  add_location
                    </i>
                <div className="Delivery-content">
                  <h3>Free Shipping</h3>
                  <p>Nulla vitae elit libero, a phare augue cum sociis natoque.</p>
                </div>
              </div>
            </Col>
            <Col md={2} className="custome-width">
              <div className="Delivery-wrapper ">
                <i class="material-icons">
                  add_location
                    </i>
                <div className="Delivery-content">
                  <h3>Free Shipping</h3>
                  <p>Nulla vitae elit libero, a phare augue cum sociis natoque.</p>
                </div>
              </div>
            </Col>
            <Col md={2} className="custome-width">
              <div className="Delivery-wrapper ">
                <i class="material-icons">
                  add_location
                    </i>
                <div className="Delivery-content">
                  <h3>Free Shipping</h3>
                  <p>Nulla vitae elit libero, a phare augue cum sociis natoque.</p>
                </div>
              </div>
            </Col>
            <Col md={2} className="custome-width">
              <div className="Delivery-wrapper ">
                <i class="material-icons">
                  add_location
                    </i>
                <div className="Delivery-content">
                  <h3>Free Shipping</h3>
                  <p>Nulla vitae elit libero, a phare augue cum sociis natoque.</p>
                </div>
              </div>
            </Col>

          </div>
        </div>
      </div>


    );
  }
}

export default DeliveryProcess;




import React from "react";
import { Row, Col, Grid, Button } from 'react-bootstrap'
import ShopCategoryInside from "../ShopCategoryInside/ShopCategoryInside";
import { addToLocalCart, updateObjectWithToken, getObjectWithToken, toCloudinary, removeFromLocalCart } from "../../services/CommonServices";
import { Link } from 'react-router-dom'
import InputNumber from 'rc-input-number';
import 'rc-input-number/assets/index.css';
import { Subscribe } from "unstated";
import CartContainer from '../../Cart'
class Cart extends React.Component {
	constructor() {
		super()
		this.deleteFromCart = this.deleteFromCart.bind(this)
		this.getPrice = this.getPrice.bind(this)
		this.getTotal = this.getTotal.bind(this)
		this.state = {
			products: [],
			productsPrices: [],
			price: "",
			total: 0,
			quantity: 1
		}
	}
	 async changeQuantity(index, n) {
		let cartProducts = this.state.products.slice();
		cartProducts[index].quantity += n;
		cartProducts[index].quantity = (cartProducts[index].quantity > 10) ? 10 : cartProducts[index].quantity;
		await this.setState({quantity: cartProducts[index].quantity});
		console.log('quantity', this.state.quantity);
		if(! localStorage.getItem("token")){
			console.log('not loggedin')
			this.getTotal();
            //addToLocalCart({ product:cartProducts[index] }, this.cart.refreshCart);
		}
		console.log('cartProducts[index]', cartProducts[index]);
		console.log('cartProducts[index].quantity',cartProducts[index].quantity);
		if (cartProducts[index].quantity > 0) {
			updateObjectWithToken('users/admin', {
				cart: {
					products: cartProducts
				}
			})
				.then((response) => {
					// console.log(response)
					this.getTotal();
					this.setState({ products: cartProducts }, () => {

						this.cart.refreshCart()
					});
				})
		}
	}

	handleQuantityChange(value, index) {
		const quantity = value;
		if (quantity)
			this.changeQuantity(index, quantity - this.state.products[index].quantity);
	}
	getPrice(product, sizeIndex, productIndex) {
		let prices = this.state.productsPrices.slice(0)
		if (prices[productIndex] !== product.sizes[sizeIndex].price) {
			prices[productIndex] = product.sizes[sizeIndex].price
			this.setState({ productsPrices: prices })
		}
		return product.sizes[sizeIndex].price.toFixed(2);
	}
	deleteFromCart(key) {
		console.log(key, "key")

		if (!localStorage.getItem("token")) {
			removeFromLocalCart(key, () => {
				this.cart.refreshCart()
				this.componentDidMount()
			});
			return;
		}
		let cartProducts = this.state.products.slice(0);
		cartProducts.splice(key, 1);
		updateObjectWithToken('users/admin', {
			cart: {
				products: cartProducts
			}
		})
			.then((response) => {
				this.cart.refreshCart()
				this.componentDidMount()
			})
	}

	getTotal() {
		let totalPrices = 0;
		for (let i = 0; i < this.state.products.length; i++) {
			totalPrices += this.state.products[i].product.sizes[this.state.products[i].size].price * this.state.products[i].quantity
		}
		this.setState({ total: totalPrices.toFixed(2) })
	}

	componentDidMount() {
		window.scroll(0, 0)
		getObjectWithToken('users/cart').then((response) => {
			this.setState({ products: response.data.cart.products, productsReady: true }, this.getTotal)
			//console.log('response.data',response.data);
		})
			.catch((err) => {
				let cart = localStorage.getItem("cart")
				if (cart) {
					cart = JSON.parse(cart)
					this.setState({ products: cart, productsReady: true }, this.getTotal)
				}
			})
		if (this.cart) {
			// this.cart.refreshCart()
		}

	}

	componentDidUpdate() {
		// window.scroll(0, 0)

	}

	render() {
		return (
			<div>
				<Subscribe to={[CartContainer]}>
					{cart => {
						this.cart = cart
						return ('')
					}}
				</Subscribe>
				<ShopCategoryInside />
				<div className="cart checkout">
					<Grid>
						<div className="checkout-content">
							<span className="cart_bag">
								<h3><i class="fa fa-shopping-bag"></i>{this.state.products.length > 0 ? this.props.strings.cart + ' (' + this.state.products.length + ')' : this.props.strings.cart}</h3>
							</span>
						</div>
						<Col style={{ background: 'white' }} md={9}>
							{this.state.products.length === 0 ?
								<div>{this.props.strings.emptyCart}</div>
								:
								this.state.products.map((product, index) => (
									<div className="cart-border">
										<Row>
											<Col md={4}>
												<div className="cart-product__image">
													<img alt="product" src={toCloudinary(product.product.images[0], null, 241) || require("../../img/Bab Makkah logo.png")}
														onError={e => {
															e.target.src = require("../../img/Bab Makkah logo.png");
														}}
													/>
												</div>
											</Col>
											<Col md={6}>
												<div className="cart-details">
													<div className="cart-category">
														<h4> {product.product.type.name ? product.product.type.name[sessionStorage.getItem("lang")] : '-'} </h4>
													</div>
													<div className="cart-item">
														<h4> {product.product.name ? product.product.name[sessionStorage.getItem("lang")] : '-'} </h4>
													</div>
													<table>
														<colgroup>
															<col width="50%" />
														</colgroup>
														{/* Group 1*/}
														<tbody>
															<tr>
																<td>{this.props.strings.size}</td><td>{product.product.sizes[product.size] ? product.product.sizes[product.size].name[sessionStorage.getItem("lang")] : '-'}</td>
															</tr>
															<tr>
																<td>{this.props.strings.color}</td><td>{product.color && product.color.name ? product.color.name[sessionStorage.getItem("lang")] : '-'}</td>
															</tr>
															<tr>
																<td>{this.props.strings.price}</td><td className="cart__total-price"> {this.getPrice(product.product, product.size, index)} SAR</td>
															</tr>
															<tr>
																<td>{this.props.strings.quantity}</td><div style={{ margin: 10 }}>
																	<InputNumber
																		min={1}
																		max={10}
																		step={1}
																		value={product.quantity}
																		onChange={(e) => this.handleQuantityChange(e, index)}
																	/>
																</div>
															</tr>
														</tbody>
													</table>
												</div>
											</Col>
											<Col md={2}>
												<div className="cart-delete">
													<a onClick={() => (this.deleteFromCart(index))}>  <i class="material-icons">delete</i></a>
												</div>
											</Col>
										</Row>
									</div>
								))}
						</Col>
						<Col style={{ background: 'white' }} md={3}>
							<div className="cart__total">
								<h5> {this.props.strings.total} </h5>
								<h3>{this.state.total} SAR </h3>
							</div>
							<Link to={{ pathname: '/checkout', state: { products: this.state.products, totalPrice: this.state.total } }} ><Button disabled={this.state.products.length === 0 ? true : false} className="main-btn cart__process">{this.props.strings.continue}</Button></Link>
						</Col>
					</Grid>
				</div>
			</div>
		);
	}
}
export default Cart;
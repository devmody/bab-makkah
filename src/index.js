import 'core-js/es6/map';
import 'core-js/es6/set';
import 'core-js/es6/promise';
import 'core-js/es6/weak-map';
import React from 'react';
import ReactDOM from 'react-dom';
// import './index.css';
// import './css/bootstrap.min.css'
// import './css/font-awesome.min.css'
// import "react-responsive-carousel/lib/styles/carousel.min.css";
import App from './App';
import * as serviceWorker from './serviceWorker';
import './css/responsive.css'
import { Provider } from 'unstated';
import CartContainer from './Cart'

let cart = new CartContainer();
ReactDOM.render(<Provider inject={[cart]}><App /></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

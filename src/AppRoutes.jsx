import React, { Component } from 'react';
import Home from './Components/Home/Home';
import Shop from './Components/Shop/Shop';
import { Switch, Route, withRouter } from 'react-router-dom';
import SingleShop from "./Components/SingleShop/SingleShop";
import Checkout from "./Components/Checkout/Checkout";
import Cart from "./Components/Cart/Cart";
import AccountSetting from "./Components/AccountSetting/AccountSetting";
import Login from "./Components/Login/Login";
import ResetPass from "./Components/Login/ResetPass";
import AccountWishlist from "./Components/AccountWishlist/AccountWishlist";
import MyOrders from "./Components/MyOrders/MyOrders";
import ViewProductsOrder from "./Components/ViewProductsOrder/ViewProductsOrder";
import PaymentValidation from "./Components/Payment/PaymentValidation";
import ReactGA from 'react-ga';
import MyLoyalty from './Components/MyLoyalty/MyLoyalty';
import VendorRegister from './Components/Login/VendorRegister';
import ContactForm from './Components/ContactForm/ContactForm';
import Terms from './Components/Terms/Terms';
import AboutUs from './Components/AboutUs/AboutUs';
import PrivacyPolicy from './Components/PrivacyPolicy/PrivacyPolicy';
import ReturnPolicy from './Components/ReturnPolicy/ReturnPolicy';
import CancellationPolicy from './Components/CancellationPolicy/CancellationPolicy';
import EstimatedDelivery from './Components/EstimatedDelivery/EstimatedDelivery';
class App extends Component {
    componentDidUpdate(prevProps) {
        this.onRouteChanged();
    }

    onRouteChanged() {
        ReactGA.pageview(window.location.pathname + window.location.search);
    }
    render() {
        return (
            <Switch>
                <Route path="/login" render={(props) => <Login {...props} strings={this.props.strings} />} />
                <Route path="/reset-password" render={(props) => <ResetPass {...props} strings={this.props.strings} />} />
                <Route path="/vendor-register" render={(props) => <VendorRegister {...props} strings={this.props.strings} />} />
                <Route path="/account-wishlist" render={(props) => <AccountWishlist {...props} strings={this.props.strings} />} />
                <Route path="/contact-form" render={(props) => <ContactForm {...props} strings={this.props.strings} />} />
                <Route path="/account-orders" render={(props) => <MyOrders {...props} strings={this.props.strings} />} />
                <Route path="/account-loyalty" render={(props) => <MyLoyalty {...props} strings={this.props.strings} />} />
                <Route path="/viewProducts/:id" render={(props) => <ViewProductsOrder {...props} strings={this.props.strings} />} />
                <Route path="/cart" render={(props) => <Cart {...props} strings={this.props.strings} />} />
                <Route path="/account" render={(props) => <AccountSetting {...props} strings={this.props.strings} />} />
                <Route path="/checkout" render={(props) => <Checkout {...props} strings={this.props.strings} />} />
                <Route path='/product/:id' render={(props) => <SingleShop {...props} onChangeCart={this.onChangeCart} strings={this.props.strings} />} />
                <Route path="/shop" render={(props) => <Shop {...props} strings={this.props.strings} />} />
                <Route path="/terms" render={(props) => <Terms {...props} strings={this.props.strings} />} />
                <Route path="/aboutus" render={(props) => <AboutUs {...props} strings={this.props.strings} />} />

                <Route path="/privacy-policy" render={(props) => <PrivacyPolicy {...props} strings={this.props.strings} />} />
                <Route path="/return-policy" render={(props) => <ReturnPolicy {...props} strings={this.props.strings} />} />
                <Route path="/cancellation-policy" render={(props) => <CancellationPolicy {...props} strings={this.props.strings} />} />
                <Route path="/estimated-delivery" render={(props) => <EstimatedDelivery {...props} strings={this.props.strings} />} />
                <Route path="/launch" render={(props) => <Home refreshFunction={this.props.refreshFunction} {...props} strings={this.props.strings} launch={true} />} />
                <Route path="/payment-validation" render={(props) => <PaymentValidation {...props} strings={this.props.strings}/>} />
                <Route path="/" render={(props) => <Home refreshFunction={this.props.refreshFunction} {...this.props} {...props} strings={this.props.strings} launch={false} />} />
            </Switch>
        );
    }
}

export default withRouter(App);

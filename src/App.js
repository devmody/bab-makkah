import React, { Component } from 'react';
import 'react-toastify/dist/ReactToastify.css';
import './App.css';
import { BrowserRouter, Switch, Route, withRouter } from 'react-router-dom';
import HeaderTop from './Components/HeaderTop/HeaderTop'
import HeaderBottom from './Components/HeaderBottom/HeaderBottom'
import Footer from './Components/Footer/Footer'
import ShopSort from "./Components/ShopSort/ShopSort";
import ShopCategory from "./Components/ShopCategory/ShopCategory";
import Launch from "./Components/Launch/Launch";
import AppRoutes from "./AppRoutes";
import { ToastContainer } from 'react-toastify';
import LocalizedStrings from 'react-localization';
import langs from './lang';
import Banner from './Banner'
import { getAllObjects, getObjectWithToken, updateObjectWithToken } from './services/CommonServices'
import ReactGA from 'react-ga';
import { Subscribe } from 'unstated'
import CartContainer from './Cart'
import * as Sentry from '@sentry/browser';

Sentry.init({
  dsn: "https://ca56fbbb11b449fa8bba8e2cde0f7c82@sentry.io/1400076"
});
ReactGA.initialize('UA-129786887-1');
class App extends Component {
  constructor() {
    super();
    let strings = new LocalizedStrings(langs);
    this.onLangChange = this.onLangChange.bind(this)
    let isOpera = (!!window.opr && !!window.opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    // Firefox 1.0+
    let isFirefox = typeof InstallTrigger !== 'undefined';

    // Safari 3.0+ "[object HTMLElementConstructor]" 
    let isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof window.safari !== 'undefined' && window.safari.pushNotification));

    // Internet Explorer 6-11
    let isIE = /*@cc_on!@*/false || !!document.documentMode;

    // Edge 20+
    let isEdge = !isIE && !!window.StyleMedia;

    // Chrome 1+
    let isChrome = !!window.chrome && !!window.chrome.webstore;
    this.state = {
      showBanner: isIE,
      strings: strings,
      lang: sessionStorage.getItem("lang") ? (sessionStorage.getItem("lang")) : ('en'),
      cartLength: undefined,
      categories: [],
      user: [],
    }
  }
  getUserLanguage = () => {
    getObjectWithToken("users/profile")
      .then((response) => {
        this.setState({ user: response.data })
        if (this.state.user.language) {

          let strings = this.state.strings
          strings.setLanguage(this.state.user.language)
          this.setState({ strings: strings, lang: this.state.user.language })
          sessionStorage.setItem("lang", this.state.user.language)
          if (this.state.user.language === "en") {
            var englishBtn1 = document.getElementById("englishBtn");
            var arabicBtn1 = document.getElementById("arabicBtn");
            englishBtn1.classList.add("active")
            englishBtn1.checked = true;
            arabicBtn1.classList.remove("active")
            arabicBtn1.checked = false;
          }
          else if (this.state.user.language === "ar") {
            var englishBtn2 = document.getElementById("englishBtn");
            var arabicBtn2 = document.getElementById("arabicBtn");
            arabicBtn2.classList.add("active")
            arabicBtn2.checked = true;
            englishBtn2.classList.remove("active")
            englishBtn2.checked = false;
          }
        }
        this.componentDidMount()

      })
      .catch((err) => {
        console.log(err)
      })
    if ("activeElement" in document)
      document.activeElement.blur();

  }

  componentDidMount() {
    if (!sessionStorage.getItem("lang")) {
      sessionStorage.setItem("lang", 'en')
    }
    // getObjectWithToken("users/profile")
    //   .then((response) => {
    //     this.setState({ user: response.data })
    //     if (this.state.user.language) {

    //       let strings = this.state.strings
    //       strings.setLanguage(this.state.user.language)
    //       this.setState({ strings: strings, lang: this.state.user.language })

    //     }


    //   })
    //   .catch((err) => {
    //     console.log(err)
    //   })



    getAllObjects('category', { lang: sessionStorage.getItem('lang') })
      .then((response) => {
        this.setState({ categories: response.data })
      })
      .catch((err) => {
        console.log(err.response.data)
      })
  }
  onLangChange(language) {
    if (this.state.user) {
      sessionStorage.setItem("lang", language)
      updateObjectWithToken("users/language", { language: language }).
        then((response) => {

        }).
        catch((err) => {
          console.log(err)
        })
    }

    else {
      sessionStorage.setItem("lang", language)
    }


    getAllObjects('category', { lang: sessionStorage.getItem('lang') })
      .then((response) => {
        this.setState({ categories: response.data })
      })
      .catch((err) => {
        console.log(err)
      })

    let strings = this.state.strings
    strings.setLanguage(language)
    this.setState({ strings: strings, lang: language })
    if ("activeElement" in document)
      document.activeElement.blur();
    this.componentDidMount()
  }
  render() {
    return (
      <React.Fragment>
        <Banner show={this.state.showBanner} />
        {this.state.lang === "ar" ? (
          <React.Fragment>
            <link rel="stylesheet" type="text/css" href={window.location.origin + '/css/rtl.css'} />
            <link rel="stylesheet" type="text/css" href={window.location.origin + '/css/style_ar.css'} />
          </React.Fragment>
        ) : ('')}
        <link rel="stylesheet" type="text/css" href={window.location.origin + '/css/responsive.css'} />
        <BrowserRouter>
          <div className="App">
            <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnVisibilityChange
              draggable={false}
            />
            <HeaderTop onLangChange={this.onLangChange} strings={this.state.strings} />
            <HeaderBottom refreshFunction={this.getUserLanguage} categories={this.state.categories} strings={this.state.strings} />
            <AppRoutes refreshFunction={this.getUserLanguage} categories={this.state.categories} strings={this.state.strings} />
            <Footer strings={this.state.strings} />
          </div>
        </BrowserRouter>
      </React.Fragment>
    );
  }
}

export default App;
